import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { HttpExceptionFilter } from './common/filters/exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  // SERVER PORT
  const port = +configService.get<number>('SERVER_PORT') || 3000;
  // SWAGGER
  const options = new DocumentBuilder()
  .setTitle('CAF')
  .setDescription('this is my description')
  .addBearerAuth(
    { type: 'http', scheme: 'bearer', bearerFormat: 'JWT', in: 'header' },
    'Authorization',
  )
  .setVersion('1.0')
  .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('/api/docs', app, document, { 
    swaggerOptions: {
      filter: true,
      showRequestDuration: true,
    }
  });

  // GLOBAL HANDLE ERROR
  app.useGlobalFilters(new HttpExceptionFilter);
  app.enableCors();
  await app.listen(port);
  console.log(`listening on port ${await app.getUrl()}`)
}
bootstrap();
