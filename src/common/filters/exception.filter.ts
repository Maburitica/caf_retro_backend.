import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Logger } from "@nestjs/common";


@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    private readonly logger : Logger 
    constructor(){
        this.logger = new Logger 
    }
    catch(exception: HttpException, host: ArgumentsHost): any {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();

        console.log(exception);

        const statusCode = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR
        const message = exception instanceof HttpException ?  exception.message || exception.message : 'Internal server error'

        const errorResponse: any = {
            statusCode,
            timestamp: new Date().toISOString(),
            path: request.url,
            method: request.method,
            errorName: exception?.name,
            message: message,
            // errors: exception.getResponse()['message']
        };

        const errorLog: any = {
            statusCode,
            timestamp: new Date().toISOString(),
            errorName: exception?.name,
            message: message,
            // errors: exception.getResponse()['message']
        };

        this.logger.log( `request method: ${request.method} request url${request.url}`, JSON.stringify(errorLog));
        response.status(statusCode).json(errorResponse);
    }
}