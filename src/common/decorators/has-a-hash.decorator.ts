import { registerDecorator, ValidationOptions } from 'class-validator';
import { getManager } from 'typeorm';

export function HasAHash(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'hasAHash',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        async validate(value: any) {
          const entityManager = getManager();
          const someQuery = await entityManager.query('SELECT * FROM users WHERE user_email = ? AND user_recovery_password IS NOT NULL', [value]);
          if(someQuery.length === 0) return false;
          return true;
        },
      },
    });
  };
}