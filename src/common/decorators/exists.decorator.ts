import { registerDecorator, ValidationOptions } from 'class-validator';
import { getManager } from 'typeorm';

export function Exists(table: string, field: string, validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'exists',
      constraints: [table, field],
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        async validate(value: any, args: any) {
          const entityManager = getManager();
          const someQuery = await entityManager.query('SELECT * FROM ?? WHERE ?? = ?', [args.constraints[0], args.constraints[1], args.value]);
          if(someQuery.length === 0) return false;
          return true;
        },
      },
    });
  };
}