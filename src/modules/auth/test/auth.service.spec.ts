import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('AuthService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let recovery: string

    describe('AuthModule', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })

      it('fails to authenticate user with an incorrect password', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'wrong' })
          .expect(401)

        expect(response.body.access_token).not.toBeDefined()
      })

      // assume test data does not include a nobody@example.com user
      it('fails to authenticate user that does not exist', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'nobody@example.com', password: 'test' })
          .expect(500)

        expect(response.body.access_token).not.toBeDefined()
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Recovery Password', () => {
      it('generate token to change password', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/recovery-password')
          .send({ email: 'admin@caf.com' })
          .expect(200)

        const data = response.body.data
        // add assertions that reflect your test data
        // expect(data).toHaveLength(3) 
      })

      it('get recovery token', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/by-email/admin@caf.com')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        recovery = data.user_recovery_password
      })

      it('validate hash', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/hash')
          .send({ hash: recovery })
          .expect(200)
      })

      it('update password', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/update-password')
          .send({ email: 'admin@caf.com', password: 'Caf12345678;&' })
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User updated")
      })

      it('revert - generate token to change password', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/recovery-password')
          .send({ email: 'admin@caf.com' })
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        // expect(data).toHaveLength(3) 
      })

      it('revert update password', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/update-password')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User updated")
      })

      // it('logout', async () => {
      //   const response = await request(app.getHttpServer())
      //     .post('/auth/logout')
      //     .set('Authorization', `Bearer ${jwtToken}`)
      //     .expect(200)

      //   const data = response.body

      //   // add assertions that reflect your test data
      //   expect(data).toHaveProperty("message", "Log out")  
      // })
    })
  })

})