import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { MailService } from '../mail/mail.service';
import { LoginDTO } from './dto/login.dto';
import { JWTPayload } from './jwt.payload';
import { RecoveryDTO } from './dto/recovery.dto';
import { HashDTO } from './dto/hash.dto';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { UserDto } from '../../modules/users/dto/user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private mailService: MailService,
    private jwtService: JwtService
  ) {}

  async validateUser(email: string, password: string): Promise<boolean> {
    const user = await this.usersService.findByEmail(email);
    if (user && user.user_active === 'NO') throw new UnauthorizedException();
    return await user.validatePassword(password);
  }

  async generateAccessToken(email: string) {
    const user = await this.usersService.findByEmail(email);
    const userToSend = {
      user_name: user.user_name,
      user_lastname: user.user_lastname,
      role_id: user.role_id,
      user_id: user.user_id,
      user_email: user.user_email
    }
    const payload: JWTPayload = { user_id: user.user_id };
    const token = this.jwtService.sign(payload);

    await this.usersService.setToken(user.user_id, token)

    return {
      access_token: token,
      user: userToSend
    };
  }

  async login(loginDTO: LoginDTO) {
    const { email, password } = loginDTO;
    const valid = await this.validateUser(email, password);
    if (!valid) {
      throw new UnauthorizedException();
    }

    return await this.generateAccessToken(email);
  }

  async validateToken() {    
    return { message: 'Token valido' };
  }

  async recoveryPassword(recoveryDTO: RecoveryDTO) {    
    const user = await this.usersService.findByEmail(recoveryDTO.email);

    let token = Buffer.from(Date.now().toString(), 'binary').toString('base64');
    token = Buffer.from(token.toString(), 'binary').toString('base64');
    await this.usersService.setRecoveryPassword(user.user_email, token);
    await this.mailService.sendUserConfirmation(user, token);
    return { message: 'Token valido' };
  }

  async hash(hashDTO: HashDTO) {    
    const user = await this.usersService.findByHash(hashDTO.hash);
    if(!user) throw new BadRequestException({message: "user doesn't exist"})
    return { message: 'Hash valido' };
  }

  async updatePassword(updatePasswordDTO: UpdatePasswordDto) {    
    const user : UpdateUserDto = {
      user_email: updatePasswordDTO.email,
      user_password: updatePasswordDTO.password
    }

    await this.usersService.updatePassword(user);
    return { message: 'User updated' };
  }

  async logout(auth: UserDto) {    
    await this.usersService.deleteToken(auth.user_id)
    return { message: 'Log out' };
  }
}