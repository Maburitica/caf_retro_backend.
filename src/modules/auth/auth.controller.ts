import { UserDto } from "../../modules/users/dto/user.dto";
import { Body, Controller, HttpCode, HttpStatus, Param, Post, UseGuards, UsePipes, ValidationPipe } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiBadRequestResponse, ApiBearerAuth, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { Auth } from "./auth.decorator";
import { AuthService } from "./auth.service";
import { LoginDTO } from "./dto/login.dto";
import { RecoveryDTO } from "./dto/recovery.dto";
import { HashDTO } from "./dto/hash.dto";
import { UpdatePasswordDto } from "./dto/update-password.dto";

@Controller('auth')
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiTags('login')
  // @UsePipes(new ValidationPipe({whitelist: true}))
  @Post('login')
  @ApiOperation({
      description: 'login'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  async login(@Body() loginDTO: LoginDTO): Promise<{ access_token: string }> {
    return await this.authService.login(loginDTO);
  }

  @ApiTags('login')
  @Post('validate-token')
  @ApiBearerAuth('Authorization')
  @UseGuards(AuthGuard(['jwt']))
  @ApiOperation({
      description: 'validate if user is logged in'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  async validateToken(@Auth() auth: UserDto): Promise<{ message: string }> {
    return await this.authService.validateToken();
  }

  @ApiTags('login')
  @Post('recovery-password')
  @ApiOperation({
      description: 'generate token to reset password'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  async recoveryPassword(@Body() recoveryDTO: RecoveryDTO): Promise<{ message: string }> {
    return await this.authService.recoveryPassword(recoveryDTO);
  }

  @ApiTags('login')
  @Post('hash')
  @ApiOperation({
      description: 'validate if user has a recovery token'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  async hash(@Body() hashDTO: HashDTO): Promise<{ message: string }> {
    return await this.authService.hash(hashDTO);
  }

  @ApiTags('login')
  @Post('update-password')
  @ApiOperation({
      description: 'update user password'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  async changePassword(@Body() updatePasswordDTO: UpdatePasswordDto): Promise<{ message: string }> {
    return await this.authService.updatePassword(updatePasswordDTO);
  }

  @ApiTags('login')
  @Post('logout')
  @ApiOperation({
      description: 'delete token'
  })
  @ApiBearerAuth('Authorization')
  @UseGuards(AuthGuard(['jwt']))
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  async logout(@Auth() auth: UserDto): Promise<{ message: string }> {
    return await this.authService.logout(auth);
  }
}