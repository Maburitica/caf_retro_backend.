import { ApiProperty } from "@nestjs/swagger";
import { Allow, IsEmail, IsIdentityCard, IsPhoneNumber, MaxLength } from "class-validator";
import { HasAHash } from "../../../common/decorators/has-a-hash.decorator";
import { IsValidPassword } from "../../../common/decorators/is-valid-password.decorator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class UpdatePasswordDto {
    @ApiProperty({
        type: String,
        description: 'user password',
        default: '',
    })
    @IsValidPassword({message: "user password must be at least 8 characters long, contain at least one number and one uppercase and lowercase letter and one special character"})
    @IsNotBlank({message: "user password can't be empty"})
    @MaxLength(70)
    password: string;
  
    @ApiProperty({
        type: String,
        description: 'user email',
        default: '',
    })
    @HasAHash({message: "user don't have a recovery token"})
    @IsNotBlank({message: "user email can't be empty"})
    @IsEmail()
    @MaxLength(50)
    email: string;
}
