import { ApiProperty } from "@nestjs/swagger";
import { IsEmail } from "class-validator";

export class HashDTO {
  @ApiProperty({
    type: String,
    description: 'hash',
    default: '',
  })
  hash: string;
}