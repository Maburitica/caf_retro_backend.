import { ApiProperty } from "@nestjs/swagger";
import { IsEmail } from "class-validator";

export class LoginDTO {
  @ApiProperty({
    type: String,
    description: 'email',
    default: '',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    type: String,
    description: 'password',
    default: '',
  })
  password: string;
}