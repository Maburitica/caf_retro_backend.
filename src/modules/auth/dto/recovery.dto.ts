import { ApiProperty } from "@nestjs/swagger";
import { IsEmail } from "class-validator";

export class RecoveryDTO {
  @ApiProperty({
    type: String,
    description: 'email',
    default: '',
  })
  @IsEmail()
  email: string;
}