import {
  createParamDecorator,
  ExecutionContext,
  ForbiddenException,
} from '@nestjs/common';
import { JWTPayload } from './jwt.payload';
import * as requestIp from 'request-ip';

export const Auth = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): Partial<JWTPayload> => {
    try {
      const request = ctx.switchToHttp().getRequest();
      request.user.user_ip = requestIp.getClientIp(request);
      return request.user;
    } catch (error) {
      throw new ForbiddenException();
    }
  }
);