import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe, Put, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from './dto/user.dto';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesEntity } from '@modules/roles/roles.entity';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('users')
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiTags('users')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @ApiOperation({
      description: 'Create new user'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createUserDto: CreateUserDto, @Auth() auth: UserDto) {
    return await this.usersService.create(createUserDto, auth);
  }

  @ApiTags('users')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get All users'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  findAll() {
    return this.usersService.findAll();
  }

  @ApiTags('users')    
  @Get(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get user By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.findById(id);
  }

  @ApiTags('users')    
  @Get('by-email/:email')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get user By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  findByEmail(@Param('email') email: string) {
    return this.usersService.findByEmail(email);
  }

  @ApiTags('users')    
  @Get('exist-identification/:identification')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Existe user by identification'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  existByIdentification(@Param('identification') identification: string) {
    return this.usersService.existUserByIdentification(identification);
  }

  @ApiTags('users')    
  @Get('by-identification/:identification')
  @Roles(1, 2)
  @ApiOperation({
      description: 'User by identification'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  findByIdentification(@Param('identification') identification: string) {
    return this.usersService.findByIdentification(identification);
  }

  @ApiTags('users')
  @Get('by-role/:role')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Existe users by role'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  findUsersByRole(@Param('role') role: number) {
    return this.usersService.findAllByRole(role);
  }

  @ApiTags('users')
  @Put(':id')
  @ApiOperation({
      description: 'Update user'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateUserDto: UpdateUserDto, @Auth() auth: UserDto) {
    return this.usersService.update(id, updateUserDto, auth);
  }

  @ApiTags('users')
  @Put('change-status/:id')
  @ApiOperation({
      description: 'Update user'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  changeStatus(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.usersService.changeStatus(id, auth);
  }

  @ApiTags('users')
  @Delete(':id')
  @ApiOperation({
      description: 'Delete user'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.usersService.remove(+id, auth);
  }
}
