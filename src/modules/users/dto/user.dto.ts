import { ApiProperty } from "@nestjs/swagger";
import { Allow, IsEmail, IsIdentityCard, IsPhoneNumber } from "class-validator";
import { Exists } from "src/common/decorators/exists.decorator";
import { IsNotBlank } from "src/common/decorators/is-not-blank.decorator";

export class UserDto {
    user_id: number;
    role_id: number;
    user_name: string;
    user_lastname: string;
    user_password: string;
    user_recovery_password: string;
    user_token: string;
    user_active: string;
    idty_id: number;
    user_id_number: string;
    user_phone_number: string;
    user_email: string;
    // other
    user_ip: string;
}
