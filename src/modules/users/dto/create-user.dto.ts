import { ApiProperty } from "@nestjs/swagger";
import { Allow, IsEmail, IsIdentityCard, IsOptional, IsPhoneNumber, MaxLength } from "class-validator";
import { IsValidPassword } from "../../../common/decorators/is-valid-password.decorator";
import { Exists } from "../../../common/decorators/exists.decorator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class CreateUserDto {
    @ApiProperty({
        type: Number,
        description: 'id role',
        default: '',
    })
    @Exists('roles', 'role_id', { message: "role doesn't exist" })
    role_id: number;

    @ApiProperty({
        type: String,
        description: 'user name',
        default: '',
    })
    @IsNotBlank({ message: "user name can't be empty" })
    @MaxLength(50)
    user_name: string;

    @ApiProperty({
        type: String,
        description: 'user last name',
        default: '',
    })
    @IsNotBlank({ message: "user last name can't be empty" })
    @MaxLength(70)
    user_lastname: string;

    @ApiProperty({
        type: String,
        enum: ['MASCULINO', 'FEMENINO'],
        description: 'user gender',
    })
    @IsNotBlank({ message: "user gender can't be empty" })
    user_gender: string;

    @ApiProperty({
        type: String,
        description: 'user password',
        default: '',
    })
    @IsOptional()
    @IsValidPassword({ message: "user password must be at least 8 characters long, contain at least one number and one uppercase and lowercase letter and one special character" })
    // @IsNotBlank({message: "user password can't be empty"})
    @MaxLength(70)
    user_password: string;

    @ApiProperty({
        type: String,
        description: 'user active',
        default: 'SI'
    })
    @IsOptional()
    // @IsNotBlank({message: "user active can't be empty"})
    user_active: string;

    @ApiProperty({
        type: Number,
        description: 'id identification type',
        default: '',
    })
    @Exists('id_types', 'idty_id', { message: "id type doesn't exist" })
    idty_id: number;

    @ApiProperty({
        type: String,
        description: 'identification number',
        default: '',
    })
    @IsNotBlank({ message: "identifacation number can't be empty" })
    // @IsIdentityCard()
    @MaxLength(20)
    user_id_number: string;

    @ApiProperty({
        type: String,
        description: 'user phone number',
        default: '',
    })
    @IsNotBlank({ message: "user phone number can't be empty" })
    // @IsPhoneNumber()
    @MaxLength(10)
    user_phone_number: string;

    @ApiProperty({
        type: String,
        description: 'user email',
        default: '',
    })
    @IsOptional()
    // @IsNotBlank({message: "user email can't be empty"})
    @IsEmail()
    @MaxLength(50)
    user_email: string;
}
