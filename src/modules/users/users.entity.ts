import { IdTypesEntity } from "../id-types/id-types.entity";
import { RolesEntity } from "../roles/roles.entity";
import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, JoinColumn, BeforeInsert, ManyToOne, OneToMany, BeforeUpdate } from "typeorm";
import * as bcrypt from 'bcrypt';
import { EnrollmentPaymentsEntity } from "../enrollment-payments/enrollment-payments.entity";
import { AssistsEntity } from "../assists/assists.entity";

@Entity({ name: 'users' }) 
export class UsersEntity {
  
  @PrimaryGeneratedColumn()
  user_id: number;

  @Column({ type: 'number' })
  role_id: number;

  @Column({ type: 'varchar', length: 50, nullable: false})
  user_name: string;

  @Column({ type: 'varchar', length: 70, nullable: false })
  user_lastname: string;

  @Column({ type: 'enum', enum: ['INDEFINIDO', 'MASCULINO', 'FEMENINO'], default: 'INDEFINIDO', nullable: false })
  user_gender: string;

  @Column({ type: 'varchar', length: 70, nullable: false })
  user_password: string;

  @Column({ type: 'varchar', length: 70, nullable: true })
  user_recovery_password: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  user_token: string;

  @Column({ type: 'enum', enum: ['SI', 'NO'], default: 'SI', nullable: false })
  user_active: string;

  @Column()
  idty_id: number;

  @Column({ type: 'varchar', length: 20, nullable: false })
  user_id_number: string;

  @Column({ type: 'varchar', length: 10, nullable: false })
  user_phone_number: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  user_email: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @ManyToOne(type => RolesEntity, role => role.role_id)
  @JoinColumn({ name: 'role_id' })
  role: RolesEntity;

  @ManyToOne(type => IdTypesEntity, idtype => idtype.idty_id)
  @JoinColumn({ name: 'idty_id' })
  idty: IdTypesEntity;

  @OneToMany(type => EnrollmentPaymentsEntity, enrollemnt_payments => enrollemnt_payments.user)
  EnrollmentPayments: EnrollmentPaymentsEntity[];

  @OneToMany(type => AssistsEntity, assist => assist.user_id)
  Assists: AssistsEntity[];

  @BeforeInsert()
  async hashPassword() {
    const salt = await bcrypt.genSalt();
    this.user_password = (this.user_password) ? await bcrypt.hash(this.user_password, salt) : this.user_password;
  }

  async validatePassword(password: string): Promise<boolean> {
    return await bcrypt.compareSync(password, this.user_password);
  }
}