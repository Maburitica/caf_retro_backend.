import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDto } from './dto/user.dto';
import { UsersEntity } from './users.entity';
import { UsersRepository } from './users.repository';
import { MailService } from '../mail/mail.service';
import * as bcrypt from 'bcrypt';
import { RolesEntity } from '../roles/roles.entity';
import { Not, IsNull } from "typeorm"

@Injectable()
export class UsersService {
  constructor(
    private logsService: LogsService,
    private mailService: MailService,
    @InjectRepository(UsersEntity) private usersRepository: UsersRepository
  ) { }

  async findByName(name: string): Promise<UsersEntity> {
    const user = await this.usersRepository.findOne({ user_name: name });
    return user;
  }

  async findByHash(hash: string): Promise<UsersEntity> {
    const user = await this.usersRepository.findOne({ user_recovery_password: hash });
    return user;
  }

  async findByEmail(email: string): Promise<UsersEntity> {
    const user = await this.usersRepository.findOne({ user_email: email });
    return user;
  }

  async findByIdentification(identification: string): Promise<UsersEntity> {
    const user = await this.usersRepository.findOne({ user_id_number: identification }, { relations: ["role", "idty", "EnrollmentPayments"] });
    return user;
  }

  async findAllByRole(role: number) {
    const users = await this.usersRepository.find({ where: { role_id: role } });
    return users;
  }

  async existUserByIdentification(identification: string): Promise<Boolean> {
    const user = await this.usersRepository.findOne({ user_id_number: identification });
    const response = (user) ? true : false
    return response;
  }

  async create(createUserDto: CreateUserDto, auth: UserDto) {
    // const exist = await this.findByName(createUserDto.user_name);
    // if(exist) throw new BadRequestException({message: 'This name already exists'})

    const userName = createUserDto.user_name.split(' ')[0][0].toUpperCase() + createUserDto.user_name.split(' ')[0].slice(1);
    //const salt = await bcrypt.genSalt();
    const password = userName + createUserDto.user_id_number + '?';
    //const pass = await bcrypt.hash(password, salt);
    createUserDto.user_password = password;
    const user = this.usersRepository.create(createUserDto);
    await this.usersRepository.save(user);
    await this.mailService.sendUserPassword(user, password);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'users',
      log_table_id: user.user_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(user),
    };
    await this.logsService.create(log);

    return { message: 'User created', id: user.user_id };
  }

  async findAll() {
    const list = await this.usersRepository.find({ relations: ["role", "idty"] });

    if (!list.length) {
      throw new NotFoundException({ message: 'The list is empty' })
    }

    return list;
  }

  async findAllMails() {
    const list = await this.usersRepository.find({
      where: { role_id: Not(1), user_email: Not(IsNull()) }
    });

    if (!list.length) {
      throw new NotFoundException({ message: 'The list is empty' })
    }

    var mails: any = []
    for (let index = 0; index < list.length; index++) {
      mails.push(list[index].user_email)
    }

    return mails;
  }

  async findById(id: number) {
    const user = await this.usersRepository.findOne(id, { relations: ["role", "idty"] });
    if (!user) {
      throw new NotFoundException({ message: "user doesn't exist" })
    }

    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto, auth: UserDto) {
    const before = await this.findById(id);
    const user = await this.findById(id);
    if (!user) throw new BadRequestException({ message: "user doesn't exist" })
    // const exist = await this.findByName(updateUserDto.user_name);
    // if(exist) throw new BadRequestException({message: "This name already exist"})
    updateUserDto.user_name ? user.user_name = updateUserDto.user_name : user.user_name = user.user_name;
    updateUserDto.user_lastname ? user.user_lastname = updateUserDto.user_lastname : user.user_lastname = user.user_lastname;
    updateUserDto.user_phone_number ? user.user_phone_number = updateUserDto.user_phone_number : user.user_phone_number = user.user_phone_number;
    updateUserDto.user_gender ? user.user_gender = updateUserDto.user_gender : user.user_gender = user.user_gender;
    updateUserDto.user_email ? user.user_email = updateUserDto.user_email : user.user_email = user.user_email;
    await this.usersRepository.save(user);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'users',
      log_table_id: user.user_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(user),
    };

    await this.logsService.create(log);

    return { message: 'User updated' };
  }

  async changeStatus(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const user = await this.findById(id);
    if (!user) throw new BadRequestException({ message: "user doesn't exist" })
    user.user_active = (user.user_active === 'SI') ? 'NO' : 'SI';
    await this.usersRepository.save(user);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'users',
      log_table_id: user.user_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(user),
    };

    await this.logsService.create(log);

    return { message: 'User updated' };
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const user = await this.findById(id);
    await this.usersRepository.remove(user);

    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'users',
      log_table_id: before.user_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };

    await this.logsService.create(log);

    return { message: 'User deleted' };
  }

  async setRecoveryPassword(user_email: string, token: string) {
    const user = await this.findByEmail(user_email);
    user.user_recovery_password = token;
    await this.usersRepository.save(user);
    return { message: 'Recovery password set' };
  }

  async updatePassword(updateUserDto: UpdateUserDto) {
    const before = await this.findByEmail(updateUserDto.user_email);
    const user = await this.findByEmail(updateUserDto.user_email);
    if (!user) throw new BadRequestException({ message: "user doesn't exist" })
    const salt = await bcrypt.genSalt();
    user.user_password = await bcrypt.hash(updateUserDto.user_password, salt);
    user.user_recovery_password = null;
    await this.usersRepository.save(user);

    return { message: 'User updated' };
  }

  async setToken(user_id, token) {
    const before = await this.findById(user_id);
    const user = await this.findById(user_id);
    if (!user) throw new BadRequestException({ message: "user doesn't exist" })
    user.user_token = token;
    await this.usersRepository.save(user);
  }

  async deleteToken(user_id) {
    const before = await this.findById(user_id);
    const user = await this.findById(user_id);
    if (!user) throw new BadRequestException({ message: "user doesn't exist" })
    user.user_token = null;
    await this.usersRepository.save(user);
  }
}
