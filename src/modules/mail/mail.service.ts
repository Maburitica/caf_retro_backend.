import { EventsEntity } from '@modules/events/events.entity';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { UsersEntity } from '../users/users.entity';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(user: UsersEntity, token: string) {
    const base = (process.env.CLIENT_ROOT) ? process.env.CLIENT_ROOT : 'http://localhost:8081'
    const url = base + '/recoveryPassword?e=' + user.user_email + '&h=' + token

    await this.mailerService.sendMail({
      to: user.user_email,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Recuperar contraseña',
      template: 'recoveryPassword', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        name: user.user_name,
        url,
      },
    });
  }

  async sendUserPassword(user: UsersEntity, password: string) {
    const base = (process.env.CLIENT_ROOT) ? process.env.CLIENT_ROOT : 'http://localhost:8081'
    const url = base + '/login';

    await this.mailerService.sendMail({
      to: user.user_email,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Bienvenid@ al CAF RETRO',
      template: 'newUser', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        user: user,
        password: password,
        url,
      },
    });
  }

  async sendAllUsersEvent(users: [], event: EventsEntity) {
    const base = (process.env.CLIENT_ROOT) ? process.env.CLIENT_ROOT : 'http://localhost:8081'
    const url = base + '/event/search'

    await this.mailerService.sendMail({
      to: users,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Nuevo Evento',
      template: 'newEvent', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        event: event,
        url,
      },
    });
  }

  async sendAllUsersEventCancel(users: [], event: EventsEntity) {
    const base = (process.env.CLIENT_ROOT) ? process.env.CLIENT_ROOT : 'http://localhost:8081'
    const url = base + '/event/search'

    await this.mailerService.sendMail({
      to: users,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Evento Cancelado',
      template: 'cancelEvent', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        event: event,
        url,
      },
    });
  }
}
