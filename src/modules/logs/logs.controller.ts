import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, HttpCode, HttpStatus, ParseIntPipe } from '@nestjs/common';
import { LogsService } from './logs.service';
import { ApiBadRequestResponse, ApiBearerAuth, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('logs')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @ApiTags('logs')
  @Get()
  @Roles(1)
  @ApiOperation({
      description: 'Get All logs'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.logsService.findAll();
  }

  @ApiTags('logs')
  @Get(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Get log By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.logsService.findById(id);
  }
}
