import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from './dto/create-log.dto';
import { LogsEntity } from './logs.entity';
import { LogsRepository } from './logs.repository';

@Injectable()
export class LogsService {
  constructor(
    @InjectRepository(LogsEntity) private logsRepository: LogsRepository
  ) {}
  
  async create(createLogDto: CreateLogDto) {
    const log = this.logsRepository.create(createLogDto);
    await this.logsRepository.save(log);
    return {message: 'log created'};
  }

  async findAll() {
    const list = await this.logsRepository.find({ relations: ["user"] });

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findById(id: number) {
    const log = await this.logsRepository.findOne(id, { relations: ["user"] });
    if(!log) {
      throw new NotFoundException({message: "log doesn't exist"})
    }

    return log;
  }
}
