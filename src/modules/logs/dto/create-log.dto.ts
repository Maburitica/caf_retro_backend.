import { ApiProperty } from "@nestjs/swagger";
import { Exists } from "src/common/decorators/exists.decorator";
import { IsNotBlank } from "src/common/decorators/is-not-blank.decorator";

export class CreateLogDto {
    @ApiProperty({
        type: Number,
        description: 'id user',
        default: '',
    })
    @Exists('users', 'user_id', { message: "user doesn't exist" })
    user_id: number;

    @ApiProperty({
        type: String,
        description: 'table that had changes',
        default: '',
    })
    @IsNotBlank({message: "log table can't be empty"})
    log_table: string;

    @ApiProperty({
        type: Number,
        description: 'id table that had changes',
        default: '',
    })
    @IsNotBlank({message: "log table id can't be empty"})
    log_table_id: number;
  
    @ApiProperty({
        type: Number,
        description: 'action',
        default: '',
    })
    @IsNotBlank({message: "action can't be empty"})
    log_action: string;

    @ApiProperty({
        type: String,
        description: 'ip from where the change was made',
        default: '',
    })
    @IsNotBlank({message: "ip can't be empty"})
    log_ip: string;

    @ApiProperty({
        type: String,
        description: 'initial information on how the change began',
        default: ''
    })
    @IsNotBlank({message: "log_before can't be empty"})
    log_before: string;
    
    @ApiProperty({
        type: String,
        description: 'final information on how the change ended',
        default: ''
    })
    @IsNotBlank({message: "log_after can't be empty"})
    log_after: string;
}