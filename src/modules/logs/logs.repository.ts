import { EntityRepository, Repository } from "typeorm";
import { LogsEntity } from "./logs.entity";

@EntityRepository(LogsEntity)
export class LogsRepository extends Repository<LogsEntity> {

}