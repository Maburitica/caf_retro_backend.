import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UsersEntity } from "../users/users.entity";

@Entity({ name: 'logs' }) 
export class LogsEntity {
  
  @PrimaryGeneratedColumn()
  log_id: number;

  @Column({ type: 'number' })
  user_id: number;

  @Column({ type: 'varchar', length: 50, nullable: false})
  log_table: string;

  @Column({ type: 'integer', nullable: false })
  log_table_id: number;

  @Column({ type: 'enum', enum: ['CREATE', 'UPDATE', 'DELETE'], nullable: false })
  log_action: string;

  @Column({ type: 'varchar', length: 70, nullable: false })
  log_ip: string;

  @Column({ type: 'text', nullable: false })
  log_before: string;

  @Column({ type: 'text', nullable: false })
  log_after: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @ManyToOne(type => UsersEntity, user => user.user_id)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;
}
