import { EntityRepository, Repository } from "typeorm";
import { PhysicalEvaluationsEntity } from "./physical-evaluations.entity";

@EntityRepository(PhysicalEvaluationsEntity)
export class PhysicalEvaluationsRepository extends Repository<PhysicalEvaluationsEntity> {

}
