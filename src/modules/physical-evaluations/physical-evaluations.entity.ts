import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UsersEntity } from "../users/users.entity";

@Entity({ name: 'physical_evaluations' })
export class PhysicalEvaluationsEntity {
  @PrimaryGeneratedColumn()
  phev_id: number;

  @Column({ type: 'number' })
  user_id: number;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  phev_date: string;

  @Column({ type: 'text', nullable: false })
  phev_physical_injuries: string;

  @Column({ type: 'text', nullable: false })
  phev_drug_use: string;

  @Column({ type: 'text', nullable: false })
  phev_psychoactive_substances_use: string;

  @Column({ type: 'varchar', length: 15, nullable: false })
  phev_height: string;

  @Column({ type: 'varchar', length: 15, nullable: false })
  phev_weight: string;

  @Column({ type: 'varchar', length: 25, nullable: false })
  phev_pulse: string;

  @Column({ type: 'text', nullable: true })
  phev_observations: string;

  @Column({ type: 'enum', enum: ['SI', 'NO'], default: 'SI', nullable: false })
  phev_active: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @OneToOne(type => UsersEntity, user => user.user_id)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;
}
