import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe, Put } from '@nestjs/common';
import { PhysicalEvaluationsService } from './physical-evaluations.service';
import { CreatePhysicalEvaluationDto } from './dto/create-physical-evaluation.dto';
import { UpdatePhysicalEvaluationDto } from './dto/update-physical-evaluation.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('physical-evaluations')
@ApiUnauthorizedResponse({ description: 'Not Authorized' })
@ApiBadRequestResponse({ description: 'Bad Request' })
@ApiInternalServerErrorResponse({ description: 'Internal Error' })
export class PhysicalEvaluationsController {
  constructor(private readonly physicalEvaluationsService: PhysicalEvaluationsService) { }

  @ApiTags('physical-evaluations')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @Post()
  @Roles(1, 2)
  @ApiOperation({
    description: 'Create new evaluation'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({ description: 'Record Created' })
  async create(@Body() createPhysicalEvaluationDto: CreatePhysicalEvaluationDto, @Auth() auth: UserDto) {
    return await this.physicalEvaluationsService.create(createPhysicalEvaluationDto, auth);
  }

  @ApiTags('physical-evaluations')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
    description: 'Get All evaluations'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Ok' })
  findAll() {
    return this.physicalEvaluationsService.findAll();
  }

  @ApiTags('physical-evaluations')
  @Get(':id')
  @Roles()
  @ApiOperation({
    description: 'Get evaluation By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Ok' })
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.physicalEvaluationsService.findById(id);
  }

  @ApiTags('physical-evaluations')
  @Get('by-user/:id')
  @Roles()
  @ApiOperation({
    description: 'Get evaluation By user Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Ok' })
  findOneByUser(@Param('id', ParseIntPipe) id: number) {
    return this.physicalEvaluationsService.findByUser(id);
  }

  @ApiTags('physical-evaluations')
  @Put(':id')
  @Roles(1, 2)
  @ApiOperation({
    description: 'Update evaluation'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Ok' })
  update(@Param('id', ParseIntPipe) id: number, @Body() updatePhysicalEvaluationDto: UpdatePhysicalEvaluationDto, @Auth() auth: UserDto) {
    return this.physicalEvaluationsService.update(id, updatePhysicalEvaluationDto, auth);
  }

  @ApiTags('physical-evaluations')
  @Delete(':id')
  @Roles(1, 2)
  @ApiOperation({
    description: 'Delete evaluation'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Ok' })
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.physicalEvaluationsService.remove(+id, auth);
  }

  @ApiTags('physical-evaluations')
  @Get('by-identification/:identification')
  @Roles(1, 2)
  @ApiOperation({
    description: 'Get evaluation By user identification'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Ok' })
  findOneByIdentification(@Param('identification') identification: string) {
    return this.physicalEvaluationsService.findByIdentification(identification);
  }
}
