import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, MaxLength } from "class-validator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";
import { Exists } from "../../../common/decorators/exists.decorator";

export class CreatePhysicalEvaluationDto {
  @ApiProperty({
    type: Number,
    description: 'id user',
    default: '',
  })
  @Exists('users', 'user_id', { message: "user doesn't exist" })
  user_id: number;

  @ApiProperty({
    type: String,
    description: 'evaluation date',
    default: '',
  })
  @IsOptional()
  phev_date: string;

  @ApiProperty({
    type: String,
    description: 'description of physical injuries',
    default: '',
  })
  @IsNotBlank({ message: "physical injuries name can't be empty" })
  phev_physical_injuries: string;

  @ApiProperty({
    type: String,
    description: 'description of drug use',
    default: '',
  })
  @IsNotBlank({ message: "drug use name can't be empty" })
  phev_drug_use: string;

  @ApiProperty({
    type: String,
    description: 'descripcion of psychoactive subtances use',
    default: '',
  })
  @IsNotBlank({ message: "psychoactive substances use name can't be empty" })
  phev_psychoactive_substances_use: string;

  @ApiProperty({
    type: String,
    description: 'height',
    default: '',
  })
  @MaxLength(15)
  phev_height: string;

  @ApiProperty({
    type: String,
    description: 'weight',
    default: '',
  })
  @MaxLength(15)
  phev_weight: string;

  @ApiProperty({
    type: String,
    description: 'pulse',
    default: '',
  })
  @MaxLength(25)
  phev_pulse: string;

  @ApiProperty({
    type: String,
    description: 'general observations',
    default: '',
  })
  @IsOptional()
  phev_observations: string;

  @ApiProperty({
    type: String,
    description: 'evaluation active',
    default: 'SI',
  })
  phev_active: string;
}
