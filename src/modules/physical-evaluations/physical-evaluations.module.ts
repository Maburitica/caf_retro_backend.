import { Module } from '@nestjs/common';
import { PhysicalEvaluationsService } from './physical-evaluations.service';
import { PhysicalEvaluationsController } from './physical-evaluations.controller';
import { LogsModule } from '../logs/logs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PhysicalEvaluationsEntity } from './physical-evaluations.entity';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    LogsModule,
    UsersModule,
    TypeOrmModule.forFeature([PhysicalEvaluationsEntity])
  ],
  controllers: [PhysicalEvaluationsController],
  providers: [PhysicalEvaluationsService]
})
export class PhysicalEvaluationsModule { }
