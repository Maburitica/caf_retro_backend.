import { UsersService } from '../../modules/users/users.service';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreatePhysicalEvaluationDto } from './dto/create-physical-evaluation.dto';
import { UpdatePhysicalEvaluationDto } from './dto/update-physical-evaluation.dto';
import { PhysicalEvaluationsEntity } from './physical-evaluations.entity';
import { PhysicalEvaluationsRepository } from './physical-evaluations.repository';

@Injectable()
export class PhysicalEvaluationsService {
  constructor(
    private logsService: LogsService,
    private usersService: UsersService,
    @InjectRepository(PhysicalEvaluationsEntity) private physicalEvaluationsRepository: PhysicalEvaluationsRepository
  ) { }

  async findByUser(user: number): Promise<PhysicalEvaluationsEntity> {
    const evaluation = await this.physicalEvaluationsRepository.findOne({ user_id: user });
    return evaluation;
  }

  async create(createPhysicalEvaluationDto: CreatePhysicalEvaluationDto, auth: UserDto) {
    const evaluation = this.physicalEvaluationsRepository.create(createPhysicalEvaluationDto);
    await this.physicalEvaluationsRepository.save(evaluation);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'physical_evaluations',
      log_table_id: evaluation.phev_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(evaluation),
    };
    await this.logsService.create(log);

    return { message: 'evaluation created', id: evaluation.phev_id };
  }

  async findAll() {
    const list = await this.physicalEvaluationsRepository.find({ relations: ["user"] });

    if (!list.length) {
      throw new NotFoundException({ message: 'The list is empty' })
    }

    return list;
  }

  async findById(id: number) {
    const evaluation = await this.physicalEvaluationsRepository.findOne(id, { relations: ["user"] });
    if (!evaluation) {
      throw new NotFoundException({ message: "evaluation doesn't exist" })
    }

    return evaluation;
  }

  async update(id: number, updatePhysicalEvaluationDto: UpdatePhysicalEvaluationDto, auth: UserDto) {
    const before = await this.findById(id);
    const evaluation = await this.findById(id);
    if (!evaluation) throw new BadRequestException({ message: "evaluation doesn't exist" })
    updatePhysicalEvaluationDto.phev_date ? evaluation.phev_date = updatePhysicalEvaluationDto.phev_date : evaluation.phev_date = evaluation.phev_date;
    updatePhysicalEvaluationDto.phev_physical_injuries ? evaluation.phev_physical_injuries = updatePhysicalEvaluationDto.phev_physical_injuries : evaluation.phev_physical_injuries = evaluation.phev_physical_injuries;
    updatePhysicalEvaluationDto.phev_drug_use ? evaluation.phev_drug_use = updatePhysicalEvaluationDto.phev_drug_use : evaluation.phev_drug_use = evaluation.phev_drug_use;
    updatePhysicalEvaluationDto.phev_psychoactive_substances_use ? evaluation.phev_psychoactive_substances_use = updatePhysicalEvaluationDto.phev_psychoactive_substances_use : evaluation.phev_psychoactive_substances_use = evaluation.phev_psychoactive_substances_use;
    updatePhysicalEvaluationDto.phev_height ? evaluation.phev_height = updatePhysicalEvaluationDto.phev_height : evaluation.phev_height = evaluation.phev_height;
    updatePhysicalEvaluationDto.phev_weight ? evaluation.phev_weight = updatePhysicalEvaluationDto.phev_weight : evaluation.phev_weight = evaluation.phev_weight;
    updatePhysicalEvaluationDto.phev_pulse ? evaluation.phev_pulse = updatePhysicalEvaluationDto.phev_pulse : evaluation.phev_pulse = evaluation.phev_pulse;
    updatePhysicalEvaluationDto.phev_observations ? evaluation.phev_observations = updatePhysicalEvaluationDto.phev_observations : evaluation.phev_observations = evaluation.phev_observations;
    updatePhysicalEvaluationDto.phev_active ? evaluation.phev_active = updatePhysicalEvaluationDto.phev_active : evaluation.phev_active = evaluation.phev_active;

    await this.physicalEvaluationsRepository.save(evaluation);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'physical_evaluations',
      log_table_id: evaluation.phev_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(evaluation),
    };

    await this.logsService.create(log);

    return { message: 'evaluation updated' };
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const evaluation = await this.findById(id);
    await this.physicalEvaluationsRepository.remove(evaluation);

    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'physical_evaluations',
      log_table_id: before.phev_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };

    await this.logsService.create(log);

    return { message: 'evaluation deleted' };
  }

  async findByIdentification(identification: string): Promise<PhysicalEvaluationsEntity> {
    const user = await this.usersService.findByIdentification(identification)

    if (!user) throw new BadRequestException({ message: 'El usuario no existe' })

    const evaluation = await this.physicalEvaluationsRepository.findOne({ user_id: user.user_id });

    if (!evaluation) throw new BadRequestException({ message: 'El usuario no tiene evaluación física' })

    return evaluation;
  }
}
