import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('PhysicalEvaluationsService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let user_id: number
    let default_identification: string = '1234567890'
    let erpa_id: number
    let phev_id: number

    describe('Log In', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Create Physical Evaluations', () => {
      it('create user with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/users')
          .send({
            role_id: 3, // ROL DE CLIENTE
            user_name: "prueba",
            user_lastname: "prueba2",
            user_email: "prueba2@gmail.com",
            user_gender: "MASCULINO",
            idty_id: 1, // TARJETA DE IDENTIDAD
            user_id_number: default_identification,
            user_phone_number: "12332123"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        user_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User created")
      })

      it('create enrollment payment with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/enrollment-payments')
          .send({
            user_id: user_id,
            enst_id: 1, // PAGO
            erpa_payment_date: new Date()
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        erpa_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_payment created")
      })

      it('create physical evaluation without valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/physical-evaluations')
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(400)
      })

      it('create physical evaluation with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/physical-evaluations')
          .send({
            user_id: user_id,
            // user_id: `${user_id}`,
            phev_date: new Date(),
            phev_physical_injuries: "no tiene",
            phev_drug_use: "no usa",
            phev_psychoactive_substances_use: "no usa",
            phev_height: "80",
            phev_weight: "50",
            phev_pulse: "20",
            phev_observations: "nada"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        phev_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "evaluation created")
      })

      it('exist user identification - error', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/exist-identification/0000000')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('exist user identification - acepted', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/exist-identification/' + default_identification)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })
    })

    describe('Update Physical Evaluation', () => {      
      it('update physical evaluation without valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/physical-evaluations/' + phev_id)
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('update physical evaluation with valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/physical-evaluations/' + phev_id)
          .send({
            phev_physical_injuries: "prueba2-actualizada"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find physical evaluation id', async () => {
        const response = await request(app.getHttpServer())
          .get('/physical-evaluations/' + phev_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("phev_physical_injuries", "prueba2-actualizada")
      })
    })

    describe('Show Physical Evaluation', () => {      
      it('find all physical evaluations', async () => {
        const response = await request(app.getHttpServer())
          .get('/physical-evaluations')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find physical evaluation by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/physical-evaluations/' + phev_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("phev_id", data.phev_id)
      })

      it('find physical evaluation by user', async () => {
        const response = await request(app.getHttpServer())
          .get('/physical-evaluations/by-user/' + user_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find physical evaluation identification', async () => {
        const response = await request(app.getHttpServer())
          .get('/physical-evaluations/by-identification/' + default_identification)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })
    })

    describe('Delete data from DB', () => {
      it('delete physical evaluation created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/physical-evaluations/' + phev_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "evaluation deleted")
      })

      it('delete enrollment payment created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/enrollment-payments/' + erpa_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_payment deleted")
      })

      it('delete user created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/users/' + user_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User deleted")
      })
    })

    // describe('LogOut', () => {
    //   it('logout', async () => {
    //     const response = await request(app.getHttpServer())
    //       .post('/auth/logout')
    //       .set('Authorization', `Bearer ${jwtToken}`)
    //       .expect(200)

    //     const data = response.body

    //     // add assertions that reflect your test data
    //     expect(data).toHaveProperty("message", "Log out")  
    //   })
    // })
  })

})