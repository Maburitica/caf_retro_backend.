import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'
import { CreateRoleDto } from '../dto/create-role.dto'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('RolesService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('CRUD', () => {
    let jwtToken: string
    let id: number

    describe('AuthModule', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('CRUD', () => {
      it('create role without data', async () => {
        const response = await request(app.getHttpServer())
          .post('/roles')
          .set('Authorization', `Bearer ${jwtToken}`)
          .send({})
          .expect(400)
        // const data = response.body.data
        // add assertions that reflect your test data
        // expect(data).toHaveLength(3) 
      })

      it('create role with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/roles')
          .set('Authorization', `Bearer ${jwtToken}`)
          .send({
            role_name: 'prueba'
          })
          .expect(201)

        const data = response.body
        id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Role created")
      })

      it('create role with the same data', async () => {
        const response = await request(app.getHttpServer())
          .post('/roles')
          .set('Authorization', `Bearer ${jwtToken}`)
          .send({
            role_name: 'prueba'
          })
          .expect(400)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "This name already exists")
      })

      it('get roles', async () => {
        const response = await request(app.getHttpServer())
          .get('/roles')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        // expect(data).toHaveProperty("message") 
      })

      it('get role by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/roles/' + id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data.role_id).toEqual(id)
      })

      it('update role', async () => {
        const response = await request(app.getHttpServer())
          .put('/roles/' + id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .send({
            role_name: 'prueba2'
          })
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Role updated")
      })

      it('delete role', async () => {
        const response = await request(app.getHttpServer())
          .delete('/roles/' + id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Role deleted")
      })
    })
  })

})