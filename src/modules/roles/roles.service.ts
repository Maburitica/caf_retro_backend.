import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { RolesEntity } from './roles.entity';
import { RolesRepository } from './roles.repository';

@Injectable()
export class RolesService {

  constructor(
    private logsService: LogsService,
    @InjectRepository(RolesEntity) private rolesRepository: RolesRepository
  ) {}

  async findByName(name: string): Promise<RolesEntity> {
    const role = await this.rolesRepository.findOne({role_name: name});
    return role;
  }

  async create(createRoleDto: CreateRoleDto, auth: UserDto) {
    const exist = await this.findByName(createRoleDto.role_name);
    if(exist) throw new BadRequestException({message: 'This name already exists'})
    const role = this.rolesRepository.create(createRoleDto);
    await this.rolesRepository.save(role);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'roles',
      log_table_id: role.role_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(role),
    };
    await this.logsService.create(log);

    return {message: 'Role created', id: role.role_id};
  }

  async findAll() {
    const list = await this.rolesRepository.find();

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findById(id: number) {
    const role = await this.rolesRepository.findOne(id);
    if(!role) {
      throw new NotFoundException({message: "Role doesn't exist"})
    }

    return role;
  }

  async update(id: number, updateRoleDto: UpdateRoleDto, auth: UserDto) {
    const before = await this.findById(id);
    const role = await this.findById(id);
    if(!role) throw new BadRequestException({message: "Role doesn't exist"})
    const exist = await this.findByName(updateRoleDto.role_name);
    if(exist) throw new BadRequestException({message: "This name already exist"})
    updateRoleDto.role_name? role.role_name = updateRoleDto.role_name : role.role_name = role.role_name;
    await this.rolesRepository.save(role);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'roles',
      log_table_id: role.role_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(role),
    };
    await this.logsService.create(log);

    return {message: 'Role updated'};
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const role = await this.findById(id);
    await this.rolesRepository.remove(role);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'roles',
      log_table_id: before.role_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };
    await this.logsService.create(log);

    return {message: 'Role deleted'};
  }
}
