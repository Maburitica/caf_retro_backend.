import { Module } from '@nestjs/common';
import { RolesService } from './roles.service';
import { RolesController } from './roles.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesEntity } from './roles.entity';
import { LogsModule } from '../logs/logs.module';

@Module({
  imports: [
    LogsModule,
    TypeOrmModule.forFeature([RolesEntity])
  ],
  controllers: [RolesController],
  providers: [RolesService]
})
export class RolesModule {}
