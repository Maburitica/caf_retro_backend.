import { Controller, Get, Post, Body, Param, Delete, HttpCode, HttpStatus, UsePipes, ValidationPipe, ParseIntPipe, Put, UseGuards } from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('roles')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @ApiTags('roles')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles(1)
  @ApiOperation({
      description: 'Create new rol'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createRoleDto: CreateRoleDto, @Auth() auth: UserDto) {
    return await this.rolesService.create(createRoleDto, auth);
  }

  @ApiTags('roles')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get All Roles'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.rolesService.findAll();
  }

  @ApiTags('roles')    
  @Get(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Get Role By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.rolesService.findById(id);
  }

  @ApiTags('roles')
  @Put(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Update role'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateRoleDto: UpdateRoleDto, @Auth() auth: UserDto) {
    return this.rolesService.update(id, updateRoleDto, auth);
  }

  @ApiTags('roles')
  @Delete(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Delete role'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.rolesService.remove(+id, auth);
  }
}
