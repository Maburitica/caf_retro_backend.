import { ApiProperty } from "@nestjs/swagger";
import { MaxLength } from "class-validator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class CreateRoleDto {
    @ApiProperty({
        type: String,
        description: 'role name',
        default: '',
    })
    @IsNotBlank({message: "role name can't be empty"})
    @MaxLength(50)
    role_name: string;
}
