import { EntityRepository, Repository } from "typeorm";
import { RolesEntity } from "./roles.entity";

@EntityRepository(RolesEntity)
export class RolesRepository extends Repository<RolesEntity> {

}