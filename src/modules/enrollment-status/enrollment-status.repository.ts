import { EntityRepository, Repository } from "typeorm";
import { EnrollmentStatusEntity } from "./enrollment-status.entity";

@EntityRepository(EnrollmentStatusEntity)
export class EnrollmentStatusRepository extends Repository<EnrollmentStatusEntity> {

}