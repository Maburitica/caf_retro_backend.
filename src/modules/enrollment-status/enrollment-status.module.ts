import { Module } from '@nestjs/common';
import { EnrollmentStatusService } from './enrollment-status.service';
import { EnrollmentStatusController } from './enrollment-status.controller';
import { LogsModule } from '../logs/logs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnrollmentStatusEntity } from './enrollment-status.entity';

@Module({
  imports: [
    LogsModule,
    TypeOrmModule.forFeature([EnrollmentStatusEntity])
  ],
  controllers: [EnrollmentStatusController],
  providers: [EnrollmentStatusService]
})
export class EnrollmentStatusModule {}
