import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreateEnrollmentStatusDto } from './dto/create-enrollment-status.dto';
import { UpdateEnrollmentStatusDto } from './dto/update-enrollment-status.dto';
import { EnrollmentStatusEntity } from './enrollment-status.entity';
import { EnrollmentStatusRepository } from './enrollment-status.repository';

@Injectable()
export class EnrollmentStatusService {
  constructor(
    private logsService: LogsService,
    @InjectRepository(EnrollmentStatusEntity) private enrollmentStatusRepository: EnrollmentStatusRepository
  ) {}

  async findByName(name: string): Promise<EnrollmentStatusEntity> {
    const role = await this.enrollmentStatusRepository.findOne({enst_name: name});
    return role;
  }

  async create(createEnrollmentStatusDto: CreateEnrollmentStatusDto, auth: UserDto) {
    const exist = await this.findByName(createEnrollmentStatusDto.enst_name);
    if(exist) throw new BadRequestException({message: 'This name already exists'})
    const enrollment_status = this.enrollmentStatusRepository.create(createEnrollmentStatusDto);
    await this.enrollmentStatusRepository.save(enrollment_status);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'enrollment_status',
      log_table_id: enrollment_status.enst_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(enrollment_status),
    };
    await this.logsService.create(log);

    return {message: 'enrollment_status created', id: enrollment_status.enst_id};
  }

  async findAll() {
    const list = await this.enrollmentStatusRepository.find();

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findById(id: number) {
    const enrollment_status = await this.enrollmentStatusRepository.findOne(id);
    if(!enrollment_status) {
      throw new NotFoundException({message: "Enrollment status doesn't exist"})
    }

    return enrollment_status;
  }

  async update(id: number, updateEnrollmentStatusDto: UpdateEnrollmentStatusDto, auth: UserDto) {
    const before = await this.findById(id);
    const enrollment_status = await this.findById(id);
    if(!enrollment_status) throw new BadRequestException({message: "Enrollment status doesn't exist"})
    const exist = await this.findByName(updateEnrollmentStatusDto.enst_name);
    if(exist) throw new BadRequestException({message: "This name already exist"})
    updateEnrollmentStatusDto.enst_name? enrollment_status.enst_name = updateEnrollmentStatusDto.enst_name : enrollment_status.enst_name = enrollment_status.enst_name;
    await this.enrollmentStatusRepository.save(enrollment_status);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'enrollment_status',
      log_table_id: enrollment_status.enst_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(enrollment_status),
    };
    await this.logsService.create(log);

    return {message: 'enrollment_status updated', id: enrollment_status.enst_id};
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const enrollment_status = await this.findById(id);
    await this.enrollmentStatusRepository.remove(enrollment_status);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'enrollment_status',
      log_table_id: before.enst_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };
    await this.logsService.create(log);

    return {message: 'enrollment_status deleted'};
  }
}
