import { ApiProperty } from "@nestjs/swagger";
import { MaxLength } from "class-validator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class CreateEnrollmentStatusDto {
    @ApiProperty({
        type: String,
        description: 'enrollment status name',
        default: '',
    })
    @IsNotBlank({message: "enrollment status name can't be empty"})
    @MaxLength(50)
    enst_name: string;
}
