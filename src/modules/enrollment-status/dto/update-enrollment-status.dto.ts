import { PartialType } from '@nestjs/swagger';
import { CreateEnrollmentStatusDto } from './create-enrollment-status.dto';

export class UpdateEnrollmentStatusDto extends PartialType(CreateEnrollmentStatusDto) {}
