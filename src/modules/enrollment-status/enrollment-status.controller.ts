import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe, Put } from '@nestjs/common';
import { EnrollmentStatusService } from './enrollment-status.service';
import { CreateEnrollmentStatusDto } from './dto/create-enrollment-status.dto';
import { UpdateEnrollmentStatusDto } from './dto/update-enrollment-status.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('enrollment-status')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class EnrollmentStatusController {
  constructor(private readonly enrollmentStatusService: EnrollmentStatusService) {}

  @ApiTags('enrollment-status')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles(1)
  @ApiOperation({
      description: 'Create new enrollment-status'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createEnrollmentStatusDto: CreateEnrollmentStatusDto, @Auth() auth: UserDto) {
    return await this.enrollmentStatusService.create(createEnrollmentStatusDto, auth);
  }

  @ApiTags('enrollment-status')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get All enrollment-status'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.enrollmentStatusService.findAll();
  }

  @ApiTags('enrollment-status')    
  @Get(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Get enrollment status By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.enrollmentStatusService.findById(id);
  }

  @ApiTags('enrollment-status')
  @Put(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Update enrollment-status'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateEnrollmentStatusDto: UpdateEnrollmentStatusDto, @Auth() auth: UserDto) {
    return this.enrollmentStatusService.update(id, updateEnrollmentStatusDto, auth);
  }

  @ApiTags('enrollment-status')
  @Delete(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Delete enrollment status'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.enrollmentStatusService.remove(+id, auth);
  }
}
