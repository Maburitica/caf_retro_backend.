import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('EnrollmentStatusService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let user_id: number
    let default_identification: string = '1234567890'
    let enst_id: number
    let dateUpdate: Date = new Date()

    describe('Log In', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Create Enrollment Status', () => {
      it('create enrollment payment without data', async () => {
        const response = await request(app.getHttpServer())
          .post('/enrollment-status')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(400)
      })

      it('create enrollment payment with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/enrollment-status')
          .send({
            enst_name: 'prueba'
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        enst_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_status created")
      })
    })

    describe('Update Enrollment Status', () => {      
      it('update Enrollment Status without valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/enrollment-status/' + enst_id)
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('update Enrollment Status with valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/enrollment-status/' + enst_id)
          .send({
            enst_name: 'prueba2'
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find Enrollment Status by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/enrollment-status/' + enst_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })
    })

    describe('Show Enrollment Status', () => {      
      it('find all enrollments status', async () => {
        const response = await request(app.getHttpServer())
          .get('/enrollment-status')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find enrollment-status by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/enrollment-status/' + enst_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("enst_id", enst_id)
      })
    })

    describe('Delete data from DB', () => {
      it('delete enrollment status created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/enrollment-status/' + enst_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_status deleted")
      })
    })

    // describe('LogOut', () => {
    //   it('logout', async () => {
    //     const response = await request(app.getHttpServer())
    //       .post('/auth/logout')
    //       .set('Authorization', `Bearer ${jwtToken}`)
    //       .expect(200)

    //     const data = response.body

    //     // add assertions that reflect your test data
    //     expect(data).toHaveProperty("message", "Log out")  
    //   })
    // })
  })

})