import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Roles } from '../../common/decorators/roles.decorator';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';
import { AssistsService } from './assists.service';
import { CreateAssistDto } from './dto/create-assist.dto';
import { GetAssistByDatesDto } from './dto/get-assist-by-dates.dto';
import { RegisterAssistDto } from './dto/register-assist.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('assists')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class AssistsController {
  constructor(private readonly assistsService: AssistsService) {}

  @ApiTags('assists')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Create new assistance'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() registerAssistDto: RegisterAssistDto, @Auth() auth: UserDto) {
    return await this.assistsService.create(registerAssistDto, auth);
  }

  @ApiTags('assists')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get All assistances'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.assistsService.findAll();
  }

  @ApiTags('assists')    
  @Get(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get asistance By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.assistsService.findById(id);
  }

  @ApiTags('assists')    
  @Get('user/:id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get asistance By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOneByUser(@Param('id', ParseIntPipe) id: number) {
    return this.assistsService.findByUser(id);
  }

  @ApiTags('assists')    
  @Post('by-user')
  @Roles(1, 2, 3)
  @ApiOperation({
      description: 'Get asistance By Id And Dates'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findByIdWithDates(@Body() getAssistByDatesDto: GetAssistByDatesDto, @Auth() auth: UserDto) {
    return this.assistsService.findByUserWithDates(auth.user_id, getAssistByDatesDto.dateInitial, getAssistByDatesDto.dateFinal);
  }

  @ApiTags('assists')
  @Delete(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Delete assistance'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.assistsService.remove(+id, auth);
  }
}
