import { UsersService } from '../../modules/users/users.service';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { AssistsEntity } from './assists.entity';
import { AssistsRepository } from './assists.repository';
import { CreateAssistDto } from './dto/create-assist.dto';
import { UpdateAssistDto } from './dto/update-assist.dto';
import { RegisterAssistDto } from './dto/register-assist.dto';
import { EnrollmentPaymentsService } from '../enrollment-payments/enrollment-payments.service';
import { Between, LessThanOrEqual, MoreThanOrEqual } from 'typeorm';
import { UsersEntity } from '../users/users.entity';

@Injectable()
export class AssistsService {
  constructor(
    private logsService: LogsService,
    private usersService: UsersService,
    private enrollmentPaymentsService: EnrollmentPaymentsService,
    @InjectRepository(AssistsEntity) private assistsRepository: AssistsRepository
  ) {}

  async findByUser(user: number): Promise<AssistsEntity> {
    const assist = await this.assistsRepository.findOne({user_id: user});
    return assist;
  }

  diffDays(d1) {
    var fechaInicio = new Date(d1).getTime();
    var fechaFin    = new Date().getTime();

    var diff = fechaFin - fechaInicio;

    return Math.trunc(diff/(1000*60*60*24));
  }

  async create(registerAssistDto: RegisterAssistDto, auth: UserDto) {
    const user = await this.usersService.findByIdentification(registerAssistDto.user_id_number);
    if (user && user.user_active === 'NO') throw new NotFoundException({message: 'The user is inactive'});

    const payment = await this.enrollmentPaymentsService.findByUser(user.user_id)
    const diffdays = this.diffDays((payment.erpa_payment_date))
    if (diffdays > 30) {
      await this.usersService.changeStatus(user.user_id, auth)
      throw new NotFoundException({message: "The user don't have active payment"})
    };

    const data : CreateAssistDto = {
      user_id: user.user_id,
      assi_date: new Date()
    }
    const assist = this.assistsRepository.create(data);
    await this.assistsRepository.save(assist);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'assists',
      log_table_id: assist.assi_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(assist),
    };
    await this.logsService.create(log);

    return {message: 'assist created', id: assist.assi_id, days_missing: diffdays};
  }

  async findAll() {
    const list = await this.assistsRepository.find({ relations: ["user"] });

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findById(id: number) {
    const assist = await this.assistsRepository.findOne(id, { relations: ["user"] });
    if(!assist) {
      throw new NotFoundException({message: "assist doesn't exist"})
    }

    return assist;
  }

  async findByUserWithDates(id: number, dateInitial: Date, dateFinal: Date) {
    const assist = await this.assistsRepository.find({
      relations: ["user"],
      where: { 
        user_id: id,
        assi_date: Between(dateInitial, dateFinal)
      }
    });
    if(!assist) {
      throw new NotFoundException({message: "assist doesn't exist"})
    }

    return assist;
  }
  
  async countByMonthByUser(id: number, dateInitial: Date, dateFinal: Date) {
    const assist = await this.assistsRepository.createQueryBuilder('assists')
      .select("DATE_FORMAT(assi_date, '%c') AS month, DATE_FORMAT(assi_date, '%Y-%m') AS date, COUNT(assists.user_id) AS count")
      .where("user_id = :id AND assists.assi_date BETWEEN :startDate AND :endDate", { id: id, startDate: dateInitial, endDate: dateFinal })        
      .groupBy("date")
      .groupBy("month")
      .orderBy('date', 'DESC')          
      .getRawMany();

    return assist;
  }

  async countByMonth(dateInitial: Date, dateFinal: Date) {
    const assist = await this.assistsRepository.createQueryBuilder('assists')
      .select("DATE_FORMAT(assi_date, '%c') AS month, DATE_FORMAT(assi_date, '%Y-%m') AS date, COUNT(assists.user_id) AS count")
      .where("assists.assi_date BETWEEN :startDate AND :endDate", { startDate: dateInitial, endDate: dateFinal })        
      .groupBy("date")
      .groupBy("month")
      .orderBy('date', 'DESC')          
      .getRawMany();

    return assist;
  }

  async countByGender(dateInitial: Date, dateFinal: Date) {
    const assist = await this.assistsRepository.createQueryBuilder('assists')           
      .select("DATE_FORMAT(assi_date, '%c') AS month, users.user_gender as gender, SUM(1) AS count")
      .leftJoin(UsersEntity, "users", "users.user_id = assists.user_id")
      .where("assists.assi_date BETWEEN :startDate AND :endDate", { startDate: dateInitial, endDate: dateFinal })        
      .groupBy("month, gender")
      // .groupBy("gender")
      .orderBy('gender', 'DESC')          
      .getRawMany();

    return assist;
  }

  async countGeneral(dateInitial: Date, dateFinal: Date) {
    const assist = await this.assistsRepository.count({
      where: {
        assi_date: Between(dateInitial, dateFinal)
      }
    });

    return assist;
  }

  async countByUser(id: number, dateInitial: Date, dateFinal: Date) {
    const assist = await this.assistsRepository.count({
      where: { 
        user_id: id,
        assi_date: Between(dateInitial, dateFinal)
      }
    });

    return assist;
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const assist = await this.findById(id);
    await this.assistsRepository.remove(assist);

    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'assists',
      log_table_id: before.assi_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };

    await this.logsService.create(log);

    return {message: 'assist deleted'};
  }
}
