import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('EnrollmentPaymentsService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let user_id: number
    let default_identification: string = '1234567890'
    let erpa_id: number
    let assi_id: number
    let dateInitial: Date = new Date()

    describe('Log In', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Create Assists', () => {
      it('create user with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/users')
          .send({
            role_id: 3, // ROL DE CLIENTE
            user_name: "prueba",
            user_lastname: "prueba2",
            user_email: "prueba2@gmail.com",
            user_gender: "MASCULINO",
            idty_id: 1, // TARJETA DE IDENTIDAD
            user_id_number: default_identification,
            user_phone_number: "12332123"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        user_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User created")
      })

      it('create enrollment payment with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/enrollment-payments')
          .send({
            user_id: user_id,
            enst_id: 1, // PAGO
            erpa_payment_date: new Date()
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        erpa_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_payment created")
      })

      it('exist user identification - error', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/exist-identification/0000000')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('exist user identification - acepted', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/exist-identification/' + default_identification)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find user by identification', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/by-identification/' + default_identification)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('create assists', async () => {
        const response = await request(app.getHttpServer())
          .post('/assists')
          .send({
            user_id_number: default_identification,
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body
        assi_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "assist created")
      })
    })

    describe('Show Assists', () => {      
      it('find all assists', async () => {
        const response = await request(app.getHttpServer())
          .get('/assists')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find assists by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/assists/' + assi_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("assi_id", data.assi_id)
      })

      it('find assists by user', async () => {
        const response = await request(app.getHttpServer())
          .get('/assists/user/' + user_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("user_id", data.user_id)
        expect(data).toHaveProperty("assi_id", data.assi_id)
      })

      it('find assists by user', async () => {
        const response = await request(app.getHttpServer())
          .post('/assists/by-user')
          .send({
            dateInitial: dateInitial,
            dateFinal: new Date()
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })
    })

    describe('Delete data from DB', () => {
      it('delete assists created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/assists/' + assi_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "assist deleted")
      })

      it('delete enrollment payment created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/enrollment-payments/' + erpa_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_payment deleted")
      })

      it('delete user created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/users/' + user_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User deleted")
      })
    })

    // describe('LogOut', () => {
    //   it('logout', async () => {
    //     const response = await request(app.getHttpServer())
    //       .post('/auth/logout')
    //       .set('Authorization', `Bearer ${jwtToken}`)
    //       .expect(200)

    //     const data = response.body

    //     // add assertions that reflect your test data
    //     expect(data).toHaveProperty("message", "Log out")  
    //   })
    // })
  })

})