import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UsersEntity } from "../users/users.entity";

@Entity({ name: 'assists' }) 
export class AssistsEntity {
  @PrimaryGeneratedColumn()
  assi_id: number;

  @Column({ type: 'number' })
  user_id: number;

  @CreateDateColumn({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP(6)" })
  assi_date: Date;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @ManyToOne(type => UsersEntity, user => user.user_id)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;
}
