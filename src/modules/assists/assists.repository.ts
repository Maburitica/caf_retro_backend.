import { EntityRepository, Repository } from "typeorm";
import { AssistsEntity } from "./assists.entity";

@EntityRepository(AssistsEntity)
export class AssistsRepository extends Repository<AssistsEntity> {

}
