import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { Exists } from "../../../common/decorators/exists.decorator";

export class RegisterAssistDto {
    @ApiProperty({
        type: String,
        description: 'id number user',
        default: '',
    })
    @Exists('users', 'user_id_number', { message: "user doesn't exist" })
    user_id_number: string;

    @ApiProperty({
        type: String,
        description: 'assistance date',
        default: '',
    })
    @IsOptional()
    assi_date: Date;
}
