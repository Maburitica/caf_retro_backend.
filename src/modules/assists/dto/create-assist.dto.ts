import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { Exists } from "../../../common/decorators/exists.decorator";

export class CreateAssistDto {
    @ApiProperty({
        type: Number,
        description: 'id user',
        default: '',
    })
    @Exists('users', 'user_id', { message: "user doesn't exist" })
    user_id: number;

    @ApiProperty({
        type: String,
        description: 'assistance date',
        default: '',
    })
    @IsOptional()
    assi_date: Date;
}
