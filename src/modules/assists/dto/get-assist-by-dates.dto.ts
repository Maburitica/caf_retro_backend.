import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { Exists } from "../../../common/decorators/exists.decorator";

export class GetAssistByDatesDto {
    @ApiProperty({
        type: String,
        description: 'assistance date initial',
        default: '',
    })
    @IsOptional()
    dateInitial: Date;

    @ApiProperty({
        type: String,
        description: 'assistance date final',
        default: '',
    })
    @IsOptional()
    dateFinal: Date;
}
