import { PartialType } from '@nestjs/swagger';
import { CreateAssistDto } from './create-assist.dto';

export class UpdateAssistDto extends PartialType(CreateAssistDto) {}
