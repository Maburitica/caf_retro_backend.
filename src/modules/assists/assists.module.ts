import { Module } from '@nestjs/common';
import { AssistsService } from './assists.service';
import { AssistsController } from './assists.controller';
import { LogsModule } from '../logs/logs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssistsEntity } from './assists.entity';
import { UsersModule } from '../users/users.module';
import { EnrollmentPaymentsModule } from '../enrollment-payments/enrollment-payments.module';

@Module({
  imports: [
    LogsModule,
    UsersModule,
    EnrollmentPaymentsModule,
    TypeOrmModule.forFeature([AssistsEntity])
  ],
  controllers: [AssistsController],
  providers: [AssistsService],
  exports: [AssistsService]
})
export class AssistsModule {}
