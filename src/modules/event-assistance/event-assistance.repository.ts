import { EntityRepository, Repository } from "typeorm";
import { EventAssistanceEntity } from "./event-assistance.entity";

@EntityRepository(EventAssistanceEntity)
export class EventAssistanceRepository extends Repository<EventAssistanceEntity> {

}
