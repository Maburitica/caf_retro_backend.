import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { EventsEntity } from "../events/events.entity";
import { UsersEntity } from "../users/users.entity";

@Entity({ name: 'event_assistance' }) 
export class EventAssistanceEntity {
  @PrimaryGeneratedColumn()
  evas_id: number;

  @Column({})
  even_id: number;

  @Column({})
  user_id: number;

  @Column({ type: 'enum', enum: ['SI', 'NO'], default: 'SI', nullable: false })
  evas_active: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @ManyToOne(type => UsersEntity, user => user.user_id)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;

  @ManyToOne(type => EventsEntity, event => event.assistance)
  @JoinColumn({ name: 'even_id' })
  event: EventsEntity;
}
  