import { Module } from '@nestjs/common';
import { EventAssistanceService } from './event-assistance.service';
import { EventAssistanceController } from './event-assistance.controller';
import { LogsModule } from '../logs/logs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventAssistanceEntity } from './event-assistance.entity';

@Module({
  imports: [
    LogsModule,
    TypeOrmModule.forFeature([EventAssistanceEntity])
  ],
  controllers: [EventAssistanceController],
  providers: [EventAssistanceService],
  exports: [EventAssistanceService]
})
export class EventAssistanceModule {}
