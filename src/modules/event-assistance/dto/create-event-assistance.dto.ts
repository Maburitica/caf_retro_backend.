import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";

export class CreateEventAssistanceDto {
    @ApiProperty({
        type: Number,
        description: 'id event',
        default: '',
    })
    @IsOptional()
    even_id: number;

    @ApiProperty({
        type: Number,
        description: 'id user',
        default: '',
    })
    @IsOptional()
    user_id: number;

    @ApiProperty({
        type: String,
        description: 'assistance active',
        default: 'SI',
    })
    @IsOptional()
    evas_active: string;
}
