import { PartialType } from '@nestjs/swagger';
import { CreateEventAssistanceDto } from './create-event-assistance.dto';

export class UpdateEventAssistanceDto extends PartialType(CreateEventAssistanceDto) {}
