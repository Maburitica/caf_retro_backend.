import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, In } from 'typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreateEventAssistanceDto } from './dto/create-event-assistance.dto';
import { UpdateEventAssistanceDto } from './dto/update-event-assistance.dto';
import { EventAssistanceEntity } from './event-assistance.entity';
import { EventAssistanceRepository } from './event-assistance.repository';

@Injectable()
export class EventAssistanceService {
  constructor(
    private logsService: LogsService,
    @InjectRepository(EventAssistanceEntity) private eventAssistanceRepository: EventAssistanceRepository
  ) {}

  async create(createEventAssistanceDto: CreateEventAssistanceDto, auth: UserDto) {
    const assistance = this.eventAssistanceRepository.create(createEventAssistanceDto);
    await this.eventAssistanceRepository.save(assistance);
    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'event_assistance',
      log_table_id: assistance.evas_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(assistance),
    };
    await this.logsService.create(log);

    return {message: 'assistance created', id: assistance.evas_id};
  }

  async findAll() {
    const list = await this.eventAssistanceRepository.find({});

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async countGeneral(dateInitial: Date, dateFinal: Date) {
    const assist = await this.eventAssistanceRepository.count({
      relations: ["event"],
      where: { 
        created_at: Between(dateInitial, dateFinal),
        event: {
          even_active: In([ 'ACTIVO', 'EXITOSO', 'SIN CUPO' ])
        }
      }
    });

    return assist;
  }

  async countByUser(id: number, dateInitial: Date, dateFinal: Date) {
    const assist = await this.eventAssistanceRepository.count({
      relations: ["event"],
      where: { 
        user_id: id,
        created_at: Between(dateInitial, dateFinal),
        event: {
          even_active: In([ 'ACTIVO', 'EXITOSO', 'SIN CUPO' ])
        }
      }
    });

    return assist;
  }

  async findById(id: number) {
    const event = await this.eventAssistanceRepository.findOne(id);
    if(!event) {
      throw new NotFoundException({message: "assistance doesn't exist"})
    }

    return event;
  }

  async findByEvent(event: number) {
    const list = await this.eventAssistanceRepository.find({ where: {even_id: event}});

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findByUser(user: number) {
    const list = await this.eventAssistanceRepository.find({ where: {user_id: user}});

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }



  async update(id: number, updateEventAssistanceDto: UpdateEventAssistanceDto, auth: UserDto) {
    const before = await this.findById(id);
    const assistance = await this.findById(id);
    if(!assistance) throw new BadRequestException({message: "assistance doesn't exist"})
    updateEventAssistanceDto.user_id? assistance.user_id = updateEventAssistanceDto.user_id : assistance.user_id = assistance.user_id;
    updateEventAssistanceDto.even_id? assistance.even_id = updateEventAssistanceDto.even_id : assistance.even_id = assistance.even_id;
    updateEventAssistanceDto.evas_active? assistance.evas_active = updateEventAssistanceDto.evas_active : assistance.evas_active = assistance.evas_active;

    await this.eventAssistanceRepository.save(assistance);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'event_assistance',
      log_table_id: assistance.evas_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(assistance),
    };

    await this.logsService.create(log);

    return {message: 'assistance updated'};
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const assistance = await this.findById(id);
    await this.eventAssistanceRepository.remove(assistance);

    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'event_assistance',
      log_table_id: before.evas_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };

    await this.logsService.create(log);

    return {message: 'assistance deleted'};
  }
}
