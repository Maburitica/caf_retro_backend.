import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe, Put } from '@nestjs/common';
import { EventAssistanceService } from './event-assistance.service';
import { CreateEventAssistanceDto } from './dto/create-event-assistance.dto';
import { UpdateEventAssistanceDto } from './dto/update-event-assistance.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('event-assistance')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class EventAssistanceController {
  constructor(private readonly eventAssistanceService: EventAssistanceService) {}

  @ApiTags('event-assistance')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles()
  @ApiOperation({
      description: 'Create new assistance'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createEventAssistanceDto: CreateEventAssistanceDto, @Auth() auth: UserDto) {
    return await this.eventAssistanceService.create(createEventAssistanceDto, auth);
  }

  @ApiTags('event-assistance')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post('by-client')
  @Roles()
  @ApiOperation({
      description: 'Create new assistance'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async createByClient(@Body() createEventAssistanceDto: CreateEventAssistanceDto, @Auth() auth: UserDto) {
    createEventAssistanceDto.evas_active = 'SI'
    createEventAssistanceDto.user_id = auth.user_id
    return await this.eventAssistanceService.create(createEventAssistanceDto, auth);
  }

  @ApiTags('event-assistance')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get All assistance'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.eventAssistanceService.findAll();
  }

  @ApiTags('event-assistance')    
  @Get(':id')
  @Roles()
  @ApiOperation({
      description: 'Get assistance By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.eventAssistanceService.findById(id);
  }

  @ApiTags('event-assistance')    
  @Get('by-user/:id')
  @Roles()
  @ApiOperation({
      description: 'Get assistance By user Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findByUser(@Param('id', ParseIntPipe) id: number) {
    return this.eventAssistanceService.findByUser(id);
  }

  @ApiTags('event-assistance')    
  @Get('by-event/:id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get assistance By event Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findByEvent(@Param('id', ParseIntPipe) id: number) {
    return this.eventAssistanceService.findByEvent(id);
  }

  @ApiTags('event-assistance')
  @Put(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Update evaluation'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateEventAssistanceDto: UpdateEventAssistanceDto, @Auth() auth: UserDto) {
    return this.eventAssistanceService.update(id, updateEventAssistanceDto, auth);
  }

  @ApiTags('event-assistance')
  @Delete(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Delete evaluation'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.eventAssistanceService.remove(+id, auth);
  }
}
