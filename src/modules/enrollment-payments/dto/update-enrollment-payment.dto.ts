import { PartialType } from '@nestjs/swagger';
import { CreateEnrollmentPaymentDto } from './create-enrollment-payment.dto';

export class UpdateEnrollmentPaymentDto extends PartialType(CreateEnrollmentPaymentDto) {}
