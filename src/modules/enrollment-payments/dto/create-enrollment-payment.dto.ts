import { ApiProperty } from "@nestjs/swagger";
import { Allow, IsEmail, IsIdentityCard, IsOptional, IsPhoneNumber, MaxLength } from "class-validator";
import { Exists } from "../../../common/decorators/exists.decorator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class CreateEnrollmentPaymentDto {
    @ApiProperty({
        type: Number,
        description: 'id user',
        default: '',
    })
    @Exists('users', 'user_id', { message: "user doesn't exist" })
    user_id: number;

    @ApiProperty({
        type: Number,
        description: 'id enrollment status',
        default: '',
    })
    @Exists('enrollment_status', 'enst_id', { message: "enrollment_status doesn't exist" })
    enst_id: number;

    @ApiProperty({
        type: String,
        description: 'date of payment',
        default: '',
    })
    @IsOptional()
    erpa_payment_date: Date;
}
