import { EntityRepository, Repository } from "typeorm";
import { EnrollmentPaymentsEntity } from "./enrollment-payments.entity";

@EntityRepository(EnrollmentPaymentsEntity)
export class EnrollmentPaymentsRepository extends Repository<EnrollmentPaymentsEntity> {

}