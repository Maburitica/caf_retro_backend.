import { Module } from '@nestjs/common';
import { EnrollmentPaymentsService } from './enrollment-payments.service';
import { EnrollmentPaymentsController } from './enrollment-payments.controller';
import { LogsModule } from '../logs/logs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnrollmentPaymentsEntity } from './enrollment-payments.entity';

@Module({
  imports: [
    LogsModule,
    TypeOrmModule.forFeature([EnrollmentPaymentsEntity])
  ],
  controllers: [EnrollmentPaymentsController],
  providers: [EnrollmentPaymentsService],
  exports: [EnrollmentPaymentsService]
})
export class EnrollmentPaymentsModule {}
