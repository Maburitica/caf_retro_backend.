import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, UseGuards, HttpCode, HttpStatus, ParseIntPipe, Put } from '@nestjs/common';
import { EnrollmentPaymentsService } from './enrollment-payments.service';
import { CreateEnrollmentPaymentDto } from './dto/create-enrollment-payment.dto';
import { UpdateEnrollmentPaymentDto } from './dto/update-enrollment-payment.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';
import { Roles } from '../../common/decorators/roles.decorator';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('enrollment-payments')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class EnrollmentPaymentsController {
  constructor(private readonly enrollmentPaymentsService: EnrollmentPaymentsService) {}

  @ApiTags('enrollment-payments')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Create new enrollment_payment'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createEnrollmentPaymentDto: CreateEnrollmentPaymentDto, @Auth() auth: UserDto) {
    return await this.enrollmentPaymentsService.create(createEnrollmentPaymentDto, auth);
  }

  @ApiTags('enrollment-payments')
  @Get()
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get All enrollemnt_payments'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.enrollmentPaymentsService.findAll();
  }

  @ApiTags('enrollment-payments')    
  @Get(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get enrollment_payment By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.enrollmentPaymentsService.findById(id);
  }

  @ApiTags('enrollment-payments')
  @Put(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Update enrollment_payment'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateEnrollmentPaymentDto: UpdateEnrollmentPaymentDto, @Auth() auth: UserDto) {
    return this.enrollmentPaymentsService.update(id, updateEnrollmentPaymentDto, auth);
  }

  @ApiTags('enrollment-payments')
  @Delete(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Delete enrollment_payment'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.enrollmentPaymentsService.remove(+id, auth);
  }
}
