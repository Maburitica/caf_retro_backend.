import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToOne } from "typeorm";
import { EnrollmentStatusEntity } from "../enrollment-status/enrollment-status.entity";
import { UsersEntity } from "../users/users.entity";

@Entity({ name: 'enrollment_payments' }) 
export class EnrollmentPaymentsEntity {
  @PrimaryGeneratedColumn()
  erpa_id: number;

  @Column({ type: 'number' })
  user_id: number;

  @Column({ type: 'number' })
  enst_id: number;

  @Column({ type: 'date', nullable: true })
  erpa_payment_date: Date;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @ManyToOne(type => UsersEntity, user => user.user_id)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;

  @ManyToOne(type => EnrollmentStatusEntity, enrollment_status => enrollment_status.enst_id)
  @JoinColumn({ name: 'enst_id' })
  enrollment_status: EnrollmentStatusEntity;
}
