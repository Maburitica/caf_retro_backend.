import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreateEnrollmentPaymentDto } from './dto/create-enrollment-payment.dto';
import { UpdateEnrollmentPaymentDto } from './dto/update-enrollment-payment.dto';
import { EnrollmentPaymentsEntity } from './enrollment-payments.entity';
import { EnrollmentPaymentsRepository } from './enrollment-payments.repository';

@Injectable()
export class EnrollmentPaymentsService {
  constructor(
    private logsService: LogsService,
    @InjectRepository(EnrollmentPaymentsEntity) private enrollmentPaymentsRepository: EnrollmentPaymentsRepository
  ) {}

  async findByUser(user: number): Promise<EnrollmentPaymentsEntity> {
    const enrollment_payments = await this.enrollmentPaymentsRepository.findOne({ where: {user_id: user}, order: { erpa_id: 'DESC' } });
    return enrollment_payments;
  }

  async create(createEnrollmentPaymentDto: CreateEnrollmentPaymentDto, auth: UserDto) {
    // const exist = await this.findByUser(createEnrollmentPaymentDto.user_id);
    // if(exist) throw new BadRequestException({message: 'There is already a pending payment'})
    const enrollment_payment = this.enrollmentPaymentsRepository.create(createEnrollmentPaymentDto);
    await this.enrollmentPaymentsRepository.save(enrollment_payment);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'enrollment_payments',
      log_table_id: enrollment_payment.erpa_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(enrollment_payment),
    };
    await this.logsService.create(log);

    return {message: 'enrollment_payment created', id: enrollment_payment.erpa_id};
  }

  async findAll() {
    const list = await this.enrollmentPaymentsRepository.find({ relations: ["user", "enrollment_status"] });

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findById(id: number) {
    const user = await this.enrollmentPaymentsRepository.findOne(id, { relations: ["user", "enrollment_status"] });
    if(!user) {
      throw new NotFoundException({message: "enrollment_payment doesn't exist"})
    }

    return user;
  }

  async update(id: number, updateEnrollmentPaymentDto: UpdateEnrollmentPaymentDto, auth: UserDto) {
    const before = await this.findById(id);
    const enrollment_payment = await this.findById(id);
    if(!enrollment_payment) throw new BadRequestException({message: "enrollment_payment doesn't exist"})
    updateEnrollmentPaymentDto.user_id? enrollment_payment.user_id = updateEnrollmentPaymentDto.user_id : enrollment_payment.user_id = enrollment_payment.user_id;
    updateEnrollmentPaymentDto.enst_id? enrollment_payment.enst_id = updateEnrollmentPaymentDto.enst_id : enrollment_payment.enst_id = enrollment_payment.enst_id;
    updateEnrollmentPaymentDto.erpa_payment_date? enrollment_payment.erpa_payment_date = updateEnrollmentPaymentDto.erpa_payment_date : enrollment_payment.erpa_payment_date = enrollment_payment.erpa_payment_date;
    await this.enrollmentPaymentsRepository.save(enrollment_payment);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'enrollment_payments',
      log_table_id: enrollment_payment.erpa_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(enrollment_payment),
    };

    await this.logsService.create(log);

    return {message: 'Enrollment_payment updated'};
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const enrollment_payment = await this.findById(id);
    await this.enrollmentPaymentsRepository.remove(enrollment_payment);

    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'enrollment_payments',
      log_table_id: before.erpa_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };

    await this.logsService.create(log);

    return {message: 'enrollment_payment deleted'};
  }
}
