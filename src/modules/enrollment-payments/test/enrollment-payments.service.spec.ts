import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('EnrollmentPaymentsService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let user_id: number
    let default_identification: string = '1234567890'
    let erpa_id: number
    let dateUpdate: Date = new Date()

    describe('Log In', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Create Enrollment Payment', () => {
      it('create user without data', async () => {
        const response = await request(app.getHttpServer())
          .post('/users')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(400)
      })

      it('create user with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/users')
          .send({
            role_id: 3, // ROL DE CLIENTE
            user_name: "prueba",
            user_lastname: "prueba2",
            user_email: "prueba2@gmail.com",
            user_gender: "MASCULINO",
            idty_id: 1, // TARJETA DE IDENTIDAD
            user_id_number: default_identification,
            user_phone_number: "12332123"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        user_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User created")
      })

      it('create enrollment payment without data', async () => {
        const response = await request(app.getHttpServer())
          .post('/enrollment-payments')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(400)
      })

      it('create enrollment payment with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/enrollment-payments')
          .send({
            user_id: user_id,
            enst_id: 1, // PAGO
            erpa_payment_date: new Date()
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        erpa_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_payment created")
      })

      it('exist user identification - error', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/exist-identification/0000000')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('exist user identification - acepted', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/exist-identification/' + default_identification)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find user by identification', async () => {
        const response = await request(app.getHttpServer())
          .get('/users/by-identification/' + default_identification)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })
    })

    describe('Update Enrollment Payment', () => {      
      it('update enrollment payment without valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/enrollment-payments/' + erpa_id)
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('update enrollment payment without valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/enrollment-payments/' + erpa_id)
          .send({
            erpa_payment_date: dateUpdate
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find enrollment-payment by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/enrollment-payments/' + erpa_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })
    })

    describe('Show Enrollment Payment', () => {      
      it('find all enrollments', async () => {
        const response = await request(app.getHttpServer())
          .get('/enrollment-payments')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find enrollment-payment by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/enrollment-payments/' + erpa_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("erpa_id", erpa_id)
      })
    })

    describe('Delete data from DB', () => {
      it('delete enrollment payment created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/enrollment-payments/' + erpa_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "enrollment_payment deleted")
      })

      it('delete user created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/users/' + user_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User deleted")
      })
    })

    // describe('LogOut', () => {
    //   it('logout', async () => {
    //     const response = await request(app.getHttpServer())
    //       .post('/auth/logout')
    //       .set('Authorization', `Bearer ${jwtToken}`)
    //       .expect(200)

    //     const data = response.body

    //     // add assertions that reflect your test data
    //     expect(data).toHaveProperty("message", "Log out")  
    //   })
    // })
  })

})