import { Controller, Body, Post, HttpCode, HttpStatus, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBadRequestResponse, ApiBearerAuth, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { StadisticsService } from './stadistics.service';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';
import { GetStadisticsByDatesDto } from './dto/get-stadistics-by-dates.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('stadistics')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class StadisticsController {
  constructor(
    private readonly stadisticsService: StadisticsService
  ) {}

  @ApiTags('Stadistics')
  @Post()
  @Roles()
  @ApiOperation({
      description: 'Get All Roles'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll(@Body() getStadisticsByDatesDto: GetStadisticsByDatesDto, @Auth() auth: UserDto) {
    return this.stadisticsService.getStadistics(getStadisticsByDatesDto, auth);
  }
}
