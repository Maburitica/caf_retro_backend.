import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { Exists } from "../../../common/decorators/exists.decorator";

export class GetStadisticsByDatesDto {
    @ApiProperty({
        type: String,
        description: 'date initial',
        default: '',
    })
    @IsOptional()
    dateInitial: Date;

    @ApiProperty({
        type: String,
        description: 'date final',
        default: '',
    })
    @IsOptional()
    dateFinal: Date;
}
