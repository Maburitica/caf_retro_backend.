import { Module } from '@nestjs/common';
import { StadisticsService } from './stadistics.service';
import { StadisticsController } from './stadistics.controller';
import { AssistsModule } from '../assists/assists.module';
import { EventsModule } from '../events/events.module';
import { EventAssistanceModule } from '../event-assistance/event-assistance.module';

@Module({
  imports: [
    AssistsModule,
    EventsModule,
    EventAssistanceModule
  ],
  controllers: [StadisticsController],
  providers: [StadisticsService]
})
export class StadisticsModule {}
