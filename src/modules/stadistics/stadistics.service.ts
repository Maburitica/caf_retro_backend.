import { AssistsService } from '../assists/assists.service';
import { EventAssistanceService } from '../event-assistance/event-assistance.service';
import { EventsService } from '../events/events.service';
import { UserDto } from '../users/dto/user.dto';
import { Injectable } from '@nestjs/common';
import { GetStadisticsByDatesDto } from './dto/get-stadistics-by-dates.dto';

@Injectable()
export class StadisticsService {
  constructor(
    private assistsService: AssistsService,
    private eventAssistanceService: EventAssistanceService,
    private eventsService: EventsService
  ) {}

  async getStadistics(getStadisticsByDatesDto: GetStadisticsByDatesDto, auth: UserDto) {
    var data: any = {
      assistances: 0,
      events: 0,
      assistancesByMonth: [],
      assistancesBySex: [],
      eventsByStatus: []
    };
    const initialDate = new Date(getStadisticsByDatesDto.dateInitial)
    const initialDay = new Date(initialDate.getFullYear(), initialDate.getMonth(), initialDate.getDate(), 0, 0, 0)
    const finaDay = new Date(getStadisticsByDatesDto.dateFinal)
    const final = new Date(finaDay.getFullYear(), finaDay.getMonth(), finaDay.getDate(), 23, 59, 59)

    if (auth.role_id === 1 || auth.role_id === 2) {
      const cAssists = await this.assistsService.countGeneral(initialDay, final)
      data.assistances = cAssists
      const cEvents = await this.eventAssistanceService.countGeneral(initialDay, final)
      data.events = cEvents
      const cAssistsBySex = await this.assistsService.countByGender(initialDay, final)
      data.assistancesBySex = cAssistsBySex
      const cAssistsByMonth = await this.assistsService.countByMonth(initialDay, final)
      data.assistancesByMonth = cAssistsByMonth
      const cEventsByStatus = await this.eventsService.countByStatus(initialDay, final)
      data.eventsByStatus = cEventsByStatus
    } else {
      const cAssists = await this.assistsService.countByUser(auth.user_id, initialDay, final)
      data.assistances = cAssists
      const cEvents = await this.eventAssistanceService.countByUser(auth.user_id, initialDay, final)
      data.events = cEvents
      const cAssistsByMonth = await this.assistsService.countByMonthByUser(auth.user_id, initialDay, final)
      data.assistancesByMonth = cAssistsByMonth
    }

    return {message: 'Stadistics', data: data};
  }
}
