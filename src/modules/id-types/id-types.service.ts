import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreateIdTypeDto } from './dto/create-id-type.dto';
import { UpdateIdTypeDto } from './dto/update-id-type.dto';
import { IdTypesEntity } from './id-types.entity';
import { IdTypesRepository } from './id-types.repository';

@Injectable()
export class IdTypesService {
  
  constructor(
    private logsService: LogsService,
    @InjectRepository(IdTypesEntity) private idTypesRepository: IdTypesRepository
  ) {}

  async findByName(name: string): Promise<IdTypesEntity> {
    const id_type = await this.idTypesRepository.findOne({idty_name: name});
    return id_type;
  }

  async create(createIdTypeDto: CreateIdTypeDto, auth: UserDto) {
    const exist = await this.findByName(createIdTypeDto.idty_name);
    if(exist) throw new BadRequestException({message: 'This name already exists'})
    const id_type = this.idTypesRepository.create(createIdTypeDto);
    await this.idTypesRepository.save(id_type);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'id_types',
      log_table_id: id_type.idty_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(id_type),
    };
    await this.logsService.create(log);

    return {message: 'Id_type created', id: id_type.idty_id};
  }

  async findAll() {
    const list = await this.idTypesRepository.find();

    if(!list.length) {
      throw new NotFoundException({message: 'The list is empty'})
    }
    
    return list;
  }

  async findById(id: number) {
    const id_type = await this.idTypesRepository.findOne(id);
    if(!id_type) {
      throw new NotFoundException({message: "Id_type doesn't exist"})
    }

    return id_type;
  }

  async update(id: number, updateIdTypeDto: UpdateIdTypeDto, auth: UserDto) {
    const before = await this.findById(id);
    const id_type = await this.findById(id);
    if(!id_type) throw new BadRequestException({message: "Id_type doesn't exist"})
    const exist = await this.findByName(updateIdTypeDto.idty_name);
    if(exist) throw new BadRequestException({message: "This name already exist"})
    updateIdTypeDto.idty_name? id_type.idty_name = updateIdTypeDto.idty_name : id_type.idty_name = id_type.idty_name;
    await this.idTypesRepository.save(id_type);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'id_types',
      log_table_id: id_type.idty_id,
      log_action: 'UPDATE ',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(id_type),
    };
    await this.logsService.create(log);

    return {message: 'Id_type updated'};
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const id_type = await this.findById(id);
    await this.idTypesRepository.remove(id_type);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'id_types',
      log_table_id: before.idty_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };
    await this.logsService.create(log);

    return {message: 'Id_type deleted'};
  }
}
