import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe, Put, UseGuards } from '@nestjs/common';
import { IdTypesService } from './id-types.service';
import { CreateIdTypeDto } from './dto/create-id-type.dto';
import { UpdateIdTypeDto } from './dto/update-id-type.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('id-types')
@ApiUnauthorizedResponse({description: 'Not Authorized'})
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class IdTypesController {
  constructor(private readonly idTypesService: IdTypesService) {}

  @ApiTags('id-types')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles(1)
  @ApiOperation({
      description: 'Create new id_type'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createIdTypeDto: CreateIdTypeDto, @Auth() auth: UserDto) {
    return await this.idTypesService.create(createIdTypeDto, auth);
  }

  @ApiTags('id-types')
  @Get()
  @Roles()
  @ApiOperation({
      description: 'Get All id_types'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.idTypesService.findAll();
  }

  @ApiTags('id-types')    
  @Get(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Get id_type By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.idTypesService.findById(id);
  }

  @ApiTags('id-types')
  @Put(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Update id_type'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateIdTypeDto: UpdateIdTypeDto, @Auth() auth: UserDto) {
    return this.idTypesService.update(id, updateIdTypeDto, auth);
  }

  @ApiTags('id-types')
  @Delete(':id')
  @Roles(1)
  @ApiOperation({
      description: 'Delete id_type'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.idTypesService.remove(+id, auth);
  }
}
