import { Module } from '@nestjs/common';
import { IdTypesService } from './id-types.service';
import { IdTypesController } from './id-types.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IdTypesEntity } from './id-types.entity';
import { LogsModule } from '../logs/logs.module';

@Module({
  imports: [
    LogsModule,
    TypeOrmModule.forFeature([IdTypesEntity])
  ],
  controllers: [IdTypesController],
  providers: [IdTypesService]
})
export class IdTypesModule {}
