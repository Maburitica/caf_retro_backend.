import { EntityRepository, Repository } from "typeorm";
import { IdTypesEntity } from "./id-types.entity";

@EntityRepository(IdTypesEntity)
export class IdTypesRepository extends Repository<IdTypesEntity> {

}