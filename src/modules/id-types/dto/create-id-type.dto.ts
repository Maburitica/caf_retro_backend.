import { ApiProperty } from "@nestjs/swagger";
import { MaxLength } from "class-validator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class CreateIdTypeDto {
    @ApiProperty({
        type: String,
        description: 'id type name',
        default: '',
    })
    @IsNotBlank({message: "id_type name can't be empty"})
    @MaxLength(50)
    idty_name: string;
}
