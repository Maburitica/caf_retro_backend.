import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('IdTypesService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let idty_id: number

    describe('Log In', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Create IdType', () => {
      it('create idtype without valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/id-types')
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(400)
      })

      it('create idtype with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/id-types')
          .send({
            idty_name: "prueba"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        idty_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Id_type created")
      })
    })

    describe('Update IdType', () => {      
      it('update idtype without valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/id-types/' + idty_id)
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('update idtype with valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/id-types/' + idty_id)
          .send({
            idty_name: "prueba2-actualizada"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find idtype id', async () => {
        const response = await request(app.getHttpServer())
          .get('/id-types/' + idty_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("idty_name", "prueba2-actualizada")
      })
    })

    describe('Show IdType', () => {      
      it('find all idtypes', async () => {
        const response = await request(app.getHttpServer())
          .get('/id-types')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find idtype by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/id-types/' + idty_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("idty_id", data.idty_id)
      })
    })

    describe('Delete data from DB', () => {
      it('delete idtype created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/id-types/' + idty_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Id_type deleted")
      })
    })

    // describe('LogOut', () => {
    //   it('logout', async () => {
    //     const response = await request(app.getHttpServer())
    //       .post('/auth/logout')
    //       .set('Authorization', `Bearer ${jwtToken}`)
    //       .expect(200)

    //     const data = response.body

    //     // add assertions that reflect your test data
    //     expect(data).toHaveProperty("message", "Log out")  
    //   })
    // })
  })

})