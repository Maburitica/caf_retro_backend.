import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { LogsModule } from '../logs/logs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventsEntity } from './events.entity';
import { MailModule } from '../mail/mail.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    LogsModule,
    MailModule,
    UsersModule,
    TypeOrmModule.forFeature([EventsEntity])
  ],
  controllers: [EventsController],
  providers: [EventsService],
  exports: [EventsService]
})
export class EventsModule {}
