import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, LoggerService } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

class TestLogger implements LoggerService {
  log(message: string) { }
  error(message: string, trace: string) { }
  warn(message: string) { }
  debug(message: string) { }
  verbose(message: string) { }
}

describe('EventsService (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useLogger(new TestLogger())
    await app.init()

    // tip: access the database connection via
    // const connection = app.get(Connection)
    // const a = connection.manager
  })

  afterAll(async () => {
    await Promise.all([
      app.close(),
    ])
  })

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!')
  })

  describe('Authentication', () => {
    let jwtToken: string
    let instructor: number
    let even_id: number

    describe('Log In', () => {
      // assume test data includes user test@example.com with password 'password'
      it('authenticates user with valid credentials and provides a jwt token', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/login')
          .send({ email: 'admin@caf.com', password: 'Caf1234&' })
          .expect(200)

        // set jwt token for use in subsequent tests
        jwtToken = response.body.access_token
        expect(jwtToken).toMatch(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // jwt regex
      })
    })

    describe('Validate Token', () => {
      it('token is active', async () => {
        const response = await request(app.getHttpServer())
          .post('/auth/validate-token')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "Token valido")
      })
    })

    describe('Create Event', () => {
      it('create instructor with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/users')
          .send({
            role_id: 2, // ROL DE CLIENTE
            user_name: "instructor",
            user_lastname: "instructor2",
            user_email: "instructor2@gmail.com",
            user_gender: "MASCULINO",
            idty_id: 1, // TARJETA DE IDENTIDAD
            user_id_number: "9292929292",
            user_phone_number: "12332124"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        instructor = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User created")
      })

      it('create event without valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/events')
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(400)
      })

      it('create event with valid data', async () => {
        const response = await request(app.getHttpServer())
          .post('/events')
          .send({
            even_name: "prueba",
            even_descrition: "evento de prueba",
            even_instructor: `${instructor}`,
            even_max_participants: "2",
            even_date: new Date()
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(201)

        const data = response.body

        even_id = data.id
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "event created")
      })
    })

    describe('Update Event', () => {      
      it('update event without valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/events/' + even_id)
          .send()
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('update event with valid data', async () => {
        const response = await request(app.getHttpServer())
          .put('/events/' + even_id)
          .send({
            even_name: "prueba2-actualizada"
          })
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find event id', async () => {
        const response = await request(app.getHttpServer())
          .get('/events/' + even_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("even_name", "prueba2-actualizada")
      })
    })

    describe('Show events', () => {      
      it('find all events', async () => {
        const response = await request(app.getHttpServer())
          .get('/events')
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
      })

      it('find event by id', async () => {
        const response = await request(app.getHttpServer())
          .get('/events/' + even_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)
        
        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("evas_id", data.evas_id)
      })
    })

    describe('Delete data from DB', () => {
      it('delete event created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/events/' + even_id)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body
        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "event deleted")
      })

      it('delete instructor created', async () => {
        const response = await request(app.getHttpServer())
          .delete('/users/' + instructor)
          .set('Authorization', `Bearer ${jwtToken}`)
          .expect(200)

        const data = response.body

        // add assertions that reflect your test data
        expect(data).toHaveProperty("message", "User deleted")
      })
    })

    // describe('LogOut', () => {
    //   it('logout', async () => {
    //     const response = await request(app.getHttpServer())
    //       .post('/auth/logout')
    //       .set('Authorization', `Bearer ${jwtToken}`)
    //       .expect(200)

    //     const data = response.body

    //     // add assertions that reflect your test data
    //     expect(data).toHaveProperty("message", "Log out")  
    //   })
    // })
  })

})