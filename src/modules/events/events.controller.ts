import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UsePipes, ValidationPipe, HttpCode, HttpStatus, ParseIntPipe, Put } from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { ApiBadRequestResponse, ApiBearerAuth, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Auth } from '../auth/auth.decorator';
import { UserDto } from '../users/dto/user.dto';

@ApiBearerAuth('Authorization')
@UseGuards(AuthGuard(['jwt']), RolesGuard)
@Controller('events')
@ApiUnauthorizedResponse({ description: 'Not Authorized' })
@ApiBadRequestResponse({description: 'Bad Request'})
@ApiInternalServerErrorResponse({description: 'Internal Error'})
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @ApiTags('events')
  @UsePipes(new ValidationPipe({whitelist: true}))
  @Post()
  @Roles(1, 2)
  @ApiOperation({
    description: 'Create new event'
  })
  @HttpCode(HttpStatus.CREATED)
  @ApiUnauthorizedResponse({description: 'Not Authorized'})
  @ApiCreatedResponse({description: 'Record Created'})
  async create(@Body() createEventDto: CreateEventDto, @Auth() auth: UserDto) {
    return await this.eventsService.create(createEventDto, auth);
  }

  @ApiTags('events')
  @Get()
  @Roles(1, 2, 3)
  @ApiOperation({
      description: 'Get All events'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findAll() {
    return this.eventsService.findAll();
  }

  @ApiTags('events')
  @Get(':id')
  @Roles(1, 2)
  @ApiOperation({
      description: 'Get event By Id'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.eventsService.findById(id);
  }

  @ApiTags('events')
  @Put(':id')
  @Roles(1, 2)
  @ApiOperation({
    description: 'Update event'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  update(@Param('id', ParseIntPipe) id: number, @Body() updateEventDto: UpdateEventDto, @Auth() auth: UserDto) {
    return this.eventsService.update(id, updateEventDto, auth);
  }

  @ApiTags('events')
  @Delete(':id')
  @Roles(1, 2)
  @ApiOperation({
    description: 'Delete event'
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Ok'})
  remove(@Param('id', ParseIntPipe) id: number, @Auth() auth: UserDto) {
    return this.eventsService.remove(+id, auth);
  }
}
