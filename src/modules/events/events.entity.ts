import { UsersEntity } from "../users/users.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { EventAssistanceEntity } from "../event-assistance/event-assistance.entity";

@Entity({ name: 'events' })
export class EventsEntity {
  @PrimaryGeneratedColumn()
  even_id: number;

  @Column({ type: 'varchar', length: 50, nullable: false })
  even_name: string

  @Column({ type: 'text', nullable: false })
  even_descrition: string;

  @Column({ type: 'int', nullable: false })
  even_instructor: number;

  @Column({ type: 'int', nullable: false })
  even_max_participants: number;

  @CreateDateColumn({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP(6)" })
  even_date: Date;

  @Column({ type: 'enum', enum: ['ACTIVO', 'CANCELADO', 'EXITOSO', 'SIN CUPO'], default: 'ACTIVO', nullable: false })
  even_active: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  created_at: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", onUpdate: "CURRENT_TIMESTAMP(6)" })
  updated_at: Date;

  @ManyToOne(type => UsersEntity, user => user.user_id)
  @JoinColumn({ name: 'even_instructor' })
  instructor: UsersEntity;

  @OneToMany(type => EventAssistanceEntity, evas => evas.event)
  assistance: EventAssistanceEntity;
}
