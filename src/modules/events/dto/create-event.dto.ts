import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { IsNotBlank } from "../../../common/decorators/is-not-blank.decorator";

export class CreateEventDto {
    @ApiProperty({
        type: String,
        description: 'event name',
        default: '',
    })
    @IsNotBlank({ message: "event name can't be empty" })
    even_name: string;

    @ApiProperty({
        type: String,
        description: 'event description',
        default: '',
    })
    @IsNotBlank({ message: "event description can't be empty" })
    even_descrition: string;

    @ApiProperty({
        type: Number,
        description: 'event instructor',
        default: '',
    })
    @IsNotBlank({ message: "event instructor can't be empty" })
    even_instructor: number;

    @ApiProperty({
        type: Number,
        description: 'event maximun of participants',
        default: '',
    })
    @IsNotBlank({ message: "event quota can't be empty" })
    even_max_participants: number;

    @ApiProperty({
        type: Date,
        description: 'event datetime',
        default: '',
    })
    @IsNotBlank({ message: "event date can't be empty" })
    even_date: Date;

    @ApiProperty({
        type: String,
        description: 'event active',
        default: 'ACTIVO'
    })
    @IsOptional()
    even_active: string;
}
