import { MailService } from '../mail/mail.service';
import { UsersService } from '../users/users.service';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { LogsService } from '../logs/logs.service';
import { UserDto } from '../users/dto/user.dto';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventsEntity } from './events.entity';
import { EventsRepository } from './events.repository';

@Injectable()
export class EventsService {
  constructor(
    private logsService: LogsService,
    private usersService: UsersService,
    private mailService: MailService,
    @InjectRepository(EventsEntity) private eventsRepository: EventsRepository
  ) { }

  async create(createEventDto: CreateEventDto, auth: UserDto) {
    const event = this.eventsRepository.create(createEventDto);
    await this.eventsRepository.save(event);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'events',
      log_table_id: event.even_id,
      log_action: 'CREATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify({}),
      log_after: JSON.stringify(event),
    };
    await this.logsService.create(log);
    const users = await this.usersService.findAllMails();
    await this.mailService.sendAllUsersEvent(users, event);
    return { message: 'event created', id: event.even_id };
  }

  async findAll() {
    const list = await this.eventsRepository.find({relations: ["assistance", "instructor"]});

    if (!list.length) {
      throw new NotFoundException({ message: 'The list is empty' })
    }

    return list;
  }

  async findById(id: number) {
    const event = await this.eventsRepository.findOne(id);
    if (!event) {
      throw new NotFoundException({ message: "event doesn't exist" })
    }

    return event;
  }

  async update(id: number, updateEventDto: UpdateEventDto, auth: UserDto) {
    const before = await this.findById(id);
    const event = await this.findById(id);
    if (!event) throw new BadRequestException({ message: "event doesn't exist" })
    updateEventDto.even_name ? event.even_name = updateEventDto.even_name : event.even_name = event.even_name;
    updateEventDto.even_descrition ? event.even_descrition = updateEventDto.even_descrition : event.even_descrition = event.even_descrition;
    updateEventDto.even_instructor ? event.even_instructor = updateEventDto.even_instructor : event.even_instructor = event.even_instructor;
    updateEventDto.even_max_participants ? event.even_max_participants = updateEventDto.even_max_participants : event.even_max_participants = event.even_max_participants;
    updateEventDto.even_date ? event.even_date = updateEventDto.even_date : event.even_date = event.even_date;
    updateEventDto.even_active ? event.even_active = updateEventDto.even_active : event.even_active = event.even_active;

    await this.eventsRepository.save(event);

    // Create log
    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'events',
      log_table_id: event.even_id,
      log_action: 'UPDATE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify(event),
    }; 

    await this.logsService.create(log);

    if (event.even_active === 'CANCELADO') {
      const users = await this.usersService.findAllMails();
      await this.mailService.sendAllUsersEventCancel(users, event);
    }

    return { message: 'event updated' };
  }

  async remove(id: number, auth: UserDto) {
    const before = await this.findById(id);
    const event = await this.findById(id);
    await this.eventsRepository.remove(event);

    const log: CreateLogDto = {
      user_id: auth.user_id,
      log_table: 'events',
      log_table_id: before.even_id,
      log_action: 'DELETE',
      log_ip: auth.user_ip,
      log_before: JSON.stringify(before),
      log_after: JSON.stringify({}),
    };

    await this.logsService.create(log);

    return { message: 'event deleted' };
  }

  async countByStatus(dateInitial: Date, dateFinal: Date) {
    const events = await this.eventsRepository.createQueryBuilder('events')           
      .select("DATE_FORMAT(even_date, '%c') AS month, even_active AS status, count(even_id) AS count")
      .where("even_date BETWEEN :startDate AND :endDate", { startDate: dateInitial, endDate: dateFinal })        
      .groupBy("month, status")
      .orderBy('status', 'DESC')          
      .getRawMany();

    return events;
  }
}
