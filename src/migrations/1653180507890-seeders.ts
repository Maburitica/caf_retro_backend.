import * as bcrypt from 'bcrypt';
import {MigrationInterface, QueryRunner} from "typeorm";

export class seeders1653180507890 implements MigrationInterface { 

    public async up(queryRunner: QueryRunner): Promise<void> {
        // roles
        queryRunner.query("INSERT INTO roles (role_name) VALUES ('ADMINISTRADOR'), ('INSTRUCTOR'), ('CLIENTE');");

        // idtypes
        queryRunner.query("INSERT INTO id_types (idty_name) VALUES ('CEDULA DE CIUDADANIA'), ('TARJETA DE IDENTIDAD'), ('CEDULA DE EXTRANJERIA'), ('PASAPORTE');");

        // enrollment status
        queryRunner.query("INSERT INTO enrollment_status (enst_name) VALUES ('PAGO'), ('NO PAGO');");

        // user
        const salt = await bcrypt.genSalt();
        const pass = await bcrypt.hash('Caf1234&', salt);
        queryRunner.query("INSERT INTO users (role_id, user_name, user_lastname, user_password, user_active, idty_id, user_id_number, user_phone_number, user_email) VALUES (1, 'ADMIN', 'CAF', '" + pass + "', 'SI', 1, '123456789', '3146800384', 'admin@caf.com');");
        queryRunner.query("INSERT INTO users (role_id, user_name, user_lastname, user_password, user_active, idty_id, user_id_number, user_phone_number, user_email) VALUES (2, 'INSTRUCTOR', 'CAF', '" + pass + "', 'SI', 1, '987654321', '3146800384', 'instructor@caf.com');");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query("DELETE users WHERE user_name IN ('ADMINISTRADOR', 'INSTRUCTOR', 'CLIENTE');");
        queryRunner.query("DELETE id_types WHERE idty_name IN ('CEDULA DE CIUDADANIA', 'TARJETA DE IDENTIDAD', 'CEDULA DE EXTRANJERIA', 'PASAPORTE');");
        queryRunner.query("DELETE enrollment_status WHERE enst_name IN ('APROBADO', 'PENDIENTE', 'CANCELADO');");
        queryRunner.query("DELETE users WHERE user_email IN ('admin@caf.com', 'instructor@caf.com');");
    }

}
