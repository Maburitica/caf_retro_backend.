import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesModule } from './modules/roles/roles.module';
import { LogsModule } from './modules/logs/logs.module';
import { IdTypesModule } from './modules/id-types/id-types.module';
import { UsersModule } from './modules/users/users.module';
import { EnrollmentStatusModule } from './modules/enrollment-status/enrollment-status.module';
import { AuthModule } from './modules/auth/auth.module';
import { EnrollmentPaymentsModule } from './modules/enrollment-payments/enrollment-payments.module';
import { AssistsModule } from './modules/assists/assists.module';
import { PhysicalEvaluationsModule } from './modules/physical-evaluations/physical-evaluations.module';
import { EventsModule } from './modules/events/events.module';
import { EventAssistanceModule } from './modules/event-assistance/event-assistance.module';
import { MailModule } from './modules/mail/mail.module';
import { StadisticsModule } from './modules/stadistics/stadistics.module';
import * as ormconfig from './ormconfig';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true
    }),
    TypeOrmModule.forRoot({
      ...ormconfig, 
      keepConnectionAlive: true, 
      autoLoadEntities: true
    }),
    RolesModule,
    LogsModule,
    IdTypesModule,
    EnrollmentStatusModule,
    UsersModule,
    AuthModule,
    EnrollmentPaymentsModule,
    AssistsModule,
    PhysicalEvaluationsModule,
    EventsModule,
    EventAssistanceModule,
    MailModule,
    StadisticsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
