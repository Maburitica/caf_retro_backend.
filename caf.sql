CREATE TABLE roles (
	role_id INT AUTO_INCREMENT NOT NULL,
  role_name VARCHAR(50),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (role_id)
);

CREATE TABLE id_types (
	idty_id INT AUTO_INCREMENT NOT NULL,
  idty_name VARCHAR(50),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (idty_id)
);

CREATE TABLE enrollment_status (
	enst_id INT AUTO_INCREMENT NOT NULL,
  enst_name VARCHAR(50),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (enst_id)
);

CREATE TABLE users (
	user_id INT AUTO_INCREMENT NOT NULL,
	role_id INT NULL,
  user_name VARCHAR(50),
  user_lastname VARCHAR(70),
  user_password VARCHAR(70),
  user_active VARCHAR(70),
  idty_id INT NULL,
  user_id_number INT(20),
  user_phone_number VARCHAR(10),
  user_email VARCHAR(50),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id),
  FOREIGN KEY (role_id) REFERENCES roles(role_id),
  FOREIGN KEY (idty_id) REFERENCES id_types(idty_id)
);

CREATE TABLE enrollment_payments (
	erpa_id INT AUTO_INCREMENT NOT NULL,
	user_id INT NULL,
	enst_id INT NULL,
  erpa_payment_date DATE,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (erpa_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id),
  FOREIGN KEY (enst_id) REFERENCES enrollment_status(enst_id)
);

CREATE TABLE assists (
	assi_id INT AUTO_INCREMENT NOT NULL,
	user_id INT NULL,
  assi_date TIMESTAMP,
  -- assi_time VARCHAR(20),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (assi_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE physical_evaluations (
	phev_id INT AUTO_INCREMENT NOT NULL,
	user_id INT NULL,
  phev_date DATE NOT NULL,
  -- phev_time VARCHAR(20) NOT NULL,
  phev_physical_injuries TEXT NOT NULL,
  phev_drug_use TEXT NOT NULL,
  phev_dream_time VARCHAR(20) NOT NULL,
  phev_psychoactive_substances_use TEXT NOT NULL,
  phev_height VARCHAR(15) NOT NULL,
  phev_weight VARCHAR(15) NOT NULL,
  phev_pulse VARCHAR(25) NOT NULL,
  phev_observations TEXT NULL,
  phev_active VARCHAR(70),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (phev_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE events (
  even_id INT AUTO_INCREMENT NOT NULL,
  even_name VARCHAR(50),
  even_descrition TEXT,
  even_max_participants INT,
  even_date DATE,
  even_time VARCHAR(20),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (even_id)
);

CREATE TABLE event_assistance (
  evas_id INT AUTO_INCREMENT NOT NULL,
  even_id INT NULL,
  user_id INT NULL,
  evas_active VARCHAR(70),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (evas_id),
  FOREIGN KEY (even_id) REFERENCES events(even_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE logs (
	log_id INT AUTO_INCREMENT NOT NULL,
  user_id INT NULL,
  log_table VARCHAR(255),
  log_table_id INT NULL,
  log_action VARCHAR(255),
  log_ip VARCHAR(255),
  log_before TEXT NULL,
  log_after TEXT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (log_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);

-- INSERTS

INSERT INTO roles (role_name) values ('ADMINISTRADOR'), ('INSTRUCTOR'), ('CLIENTE');

INSERT INTO id_types (idty_name) values ('CEDULA DE CIUDADANIA'), ('TARJETA DE IDENTIDAD'), ('CEDULA DE EXTRANJERIA'), ('PASAPORTE');

INSERT INTO enrollment_status (enst_name) values ('APROBADO'), ('PENDIENTE'), ('CANCELADO');
